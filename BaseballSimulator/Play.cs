﻿using System;
using System.Collections.Generic;

namespace BaseballSimulator
{
	public struct Play
	{

		public int inning;
		public InningStatus inningStatus;
		public int initialOutCount;
		public int? afterOutCount;
		public Dictionary<TeamType, int> score; // <TeamType, 点数>
		public Dictionary<Base, int?> runner; // <Base, ID>

		public BattingOrder battingOrder;
		public Position position;
        public int batterID;
        public int pitcherID;
        public List<ChangingOrderStep> fieldingCommand;
        public ChangingPlayer? reliefPitcher;
        public ChangingPlayer? pinchHitter;
        public TeamStrategy teamStrategy;
        public ResultPlay? resultPlay;

        public Play(int inning, InningStatus inningStatus, int initialOutCount, Dictionary<TeamType, int> score, Dictionary<Base, int?> runner, BattingOrder battingOrder, Position position, int batterID, int pitcherID, List<ChangingOrderStep> fieldingCommand, ChangingPlayer? reliefPitcher, ChangingPlayer? pinchHitter, TeamStrategy teamStrategy)
        {
            this.inning = inning;
            this.inningStatus = inningStatus;
            this.initialOutCount = initialOutCount;
            this.score = score;
            this.runner = runner;
            this.battingOrder = battingOrder;
            this.position = position;
            this.batterID = batterID;
            this.pitcherID = pitcherID;
            this.fieldingCommand = fieldingCommand;
            this.reliefPitcher = reliefPitcher;
            this.pinchHitter = pinchHitter;
            this.teamStrategy = teamStrategy;

            this.afterOutCount = null;
            this.resultPlay = null;
        }

        public void AddParam(int afterOutCount, ResultPlay resultPlay)
        {
            this.afterOutCount = afterOutCount;
            this.resultPlay = resultPlay;
        }

	}
}

