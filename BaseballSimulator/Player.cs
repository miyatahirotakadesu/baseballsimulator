﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace BaseballSimulator
{
	[JsonObject("player")]
	public class Player
	{

		[JsonProperty("id")]
		public int id;
		[JsonProperty("name")]
		public string name;
		[JsonProperty("batting_status")]
		public BattingStatus? battingStatus;
		[JsonProperty("pitching_status")]
		public PitchingStatus? pitchingStatus;
		[JsonProperty("fieldable_positions")]
		public List<Position> fieldablePositions;
		[JsonProperty("batting_result")]
		public PlayerBattingResult battingResult;
		[JsonProperty("pitching_result")]
		public PlayerPitchingResult pitchingResult;
		[JsonProperty("game_participation")]
		public GameParticipation? game_participation;
		private BenchRole? role; // 先発出場ならNULL

		public Player (int id, string name, BattingStatus? battingStatus, PitchingStatus? pitchingStatus, List<Position> fieldablePositions)
        {
			this.id = id;
			this.name = name;
			this.fieldablePositions = fieldablePositions;

            if (battingStatus.HasValue) {
                this.battingStatus = (BattingStatus)battingStatus;
            } else {
                this.battingStatus = (this.IsFielder()) ? BattingStatus.GetBattingStatusForReplacement() : BattingStatus.GetBattingStatusForPitcher();
            }

            if (pitchingStatus.HasValue) {
                this.pitchingStatus = pitchingStatus;
            } else {
                this.pitchingStatus = (PitchingStatus?)PitchingStatus.GetPitchingStatusForReplacement(true);
            }

            this.battingResult = new PlayerBattingResult();
            this.pitchingResult = new PlayerPitchingResult();
		}

        public bool IsStarter()
        {
            return !role.HasValue;
        }

        public void SetStarterRole()
        {
            this.role = null;
        }

        public void SetRole(BenchRole role)
        {
            this.role = role;
        }

        public void ParticipateGame(int inning, BattingOrder batting_order)
        {
            this.game_participation = new GameParticipation(inning, batting_order);
        }

        public bool IsPitcher()
        {
            return fieldablePositions.Contains(Position.pitcher);
        }

        public bool IsFielder()
        {
            return fieldablePositions.Where(position => position != Position.pitcher).Count() > 0;

        }

        public void SetBattingResult(ResultPlay resultPlay, int inning)
        {
            if (!battingResult.highrights.ContainsKey(inning)) {
                battingResult.highrights.Add(inning, new List<ResultPlay>());
            }

            battingResult.highrights[inning].Add(resultPlay);

            battingResult.plate_appearance++;
            battingResult.at_bats++;
            switch (resultPlay.resultType) {
                case ResultType.single: battingResult.single++;battingResult.hits++;break;
                case ResultType.twobase: battingResult.twobase++;battingResult.hits++;break;
                case ResultType.triple: battingResult.triple++;battingResult.hits++;break;
                case ResultType.homerun: battingResult.homerun++;battingResult.hits++;break;
                case ResultType.fourball: battingResult.fourball++;battingResult.at_bats--;break;
                case ResultType.strikeout: battingResult.strikeout++;break;
                case ResultType.groundout: battingResult.groundout++;break;
                case ResultType.flyout: battingResult.flyout++;break;
                case ResultType.sacrifice_bunt: battingResult.sacrifice_bunt++;battingResult.at_bats--;break;
                case ResultType.sacrifice_fly: battingResult.sacrifice_fly++;battingResult.at_bats--;break;
                case ResultType.doubleplay: battingResult.doubleplay++;break;
                case ResultType.foulfly: battingResult.flyout++;break;
            }

            if (resultPlay.resultType != ResultType.doubleplay) {
                battingResult.rbi += resultPlay.scored;
            }

        }

        public void SetPitchingResult(ResultPlay resultPlay, int getout, int allowed_runs, int earned_runs)
        {
            pitchingResult.plate_appearance++;
            pitchingResult.getout += getout;
            pitchingResult.allowed_runs += allowed_runs;
            pitchingResult.earned_runs += earned_runs;

            switch (resultPlay.resultType) {
                case ResultType.single: pitchingResult.allowed_singles++;break;
                case ResultType.twobase: pitchingResult.allowed_doubles++;break;
                case ResultType.triple: pitchingResult.allowed_triples++;break;
                case ResultType.homerun: pitchingResult.allowed_homeruns++;break;
                case ResultType.fourball: pitchingResult.bases_on_balls++;break;
                case ResultType.strikeout: pitchingResult.strikeouts++;break;
                case ResultType.groundout: pitchingResult.groundouts++;break;
                case ResultType.flyout: pitchingResult.flyouts++;break;
                case ResultType.sacrifice_bunt: pitchingResult.allowed_sacrifice_bunts++;break;
                case ResultType.sacrifice_fly: pitchingResult.allowed_sacrifice_flies++;break;
                case ResultType.doubleplay: pitchingResult.doubleplays++;break;
                case ResultType.foulfly: pitchingResult.flyouts++;break;
            }

            pitchingResult.allowed_hits = pitchingResult.allowed_singles + pitchingResult.allowed_doubles + pitchingResult.allowed_triples + pitchingResult.allowed_homeruns;
        }

        [OnDeserialized]
        private void OnDeserialized(StreamingContext context)
        {
            if (battingStatus == null) {
                // TODO
                battingStatus = BattingStatus.GetBattingStatusForPitcher();
            }
        }

	}

	[JsonObject("game_participation")]
	public struct GameParticipation
	{
		[JsonProperty("inning")]
        public int inning; // 試合に出場したイニング
		[JsonProperty("batting_order")]
        public BattingOrder batting_order; // 試合に出場したときの打順

        public GameParticipation(int inning, BattingOrder batting_order)
        {
            this.inning = inning;
            this.batting_order = batting_order;
        }
    }

}

