﻿using System;
using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace BaseballSimulator
{
    // 代打や守備交替決定に利用
	public class ChangingOrderProcessNode
	{

        public ChangingOrderProcessNode root; // 一番上の親
        private bool isChild; // 一番上の親
        public List<ChangingOrderProcessNode> children = new List<ChangingOrderProcessNode>();
        public ChangingOrderProcess history;
        public List<ChangingOrderProcess> changingOrderOptions = new List<ChangingOrderProcess>();
        private bool hasProblem;

        public List<Player> players;
        public Dictionary<int, Player> player_map;
		public Dictionary<BattingOrder, int> order;
		public Dictionary<Position, BattingOrder> positions;
		public Dictionary<int, BenchRole> benchPlayerIds;

        private struct TeamStatus
        {
            public Dictionary<BattingOrder, int> order;
            public Dictionary<Position, BattingOrder> positions;
            public Dictionary<int, BenchRole> benchPlayerIds;
            
            public TeamStatus(Dictionary<BattingOrder, int> order, Dictionary<Position, BattingOrder> positions, Dictionary<int, BenchRole> benchPlayerIds) {
                // ディープコピー
                this.order = order.ToDictionary(entry => entry.Key, entry => entry.Value);
                this.positions = positions.ToDictionary(entry => entry.Key, entry => entry.Value);
                this.benchPlayerIds = benchPlayerIds.ToDictionary(entry => entry.Key, entry => entry.Value);
            }
        }

        // (入力)オーダーとベンチとSteps→(出力)次の候補Step一覧を返す or 次の候補Step一覧が０件 or 守備位置補正の完了連絡
        public ChangingOrderProcessNode(List<Player> players, Dictionary<int, Player> player_map, ChangingOrderProcess history, Dictionary<BattingOrder, int> order, Dictionary<Position, BattingOrder> positions, Dictionary<int, BenchRole> benchPlayerIds)
        {
            // 最上位(root)のノードはここで生成
            // Console.WriteLine("Created Parent");
            this.root = this;
            this.isChild = false;
            this.ConstructorCommon(players, player_map, history, order, positions,  benchPlayerIds);
        }

        private ChangingOrderProcessNode(ChangingOrderProcessNode root, List<Player> players, Dictionary<int, Player> player_map, ChangingOrderProcess history, Dictionary<BattingOrder, int> order, Dictionary<Position, BattingOrder> positions, Dictionary<int, BenchRole> benchPlayerIds)
        {
            // 最上位以外のノードはここで生成
            // Console.WriteLine("Created Child");
            this.root = root;
            this.isChild = true;
            this.ConstructorCommon(players, player_map, history, order, positions, benchPlayerIds);
        }

        public void ConstructorCommon(List<Player> players, Dictionary<int, Player> player_map, ChangingOrderProcess history, Dictionary<BattingOrder, int> order, Dictionary<Position, BattingOrder> positions, Dictionary<int, BenchRole> benchPlayerIds)
        {
            this.players = players;
            this.player_map = player_map;

            this.history = history.Clone(); // ディープコピー
            // Console.WriteLine("history:" + JsonConvert.SerializeObject(this.history, Formatting.Indented));
            this.order = order.ToDictionary(entry => entry.Key, entry => entry.Value); // ディープコピー
            this.positions = positions.ToDictionary(entry => entry.Key, entry => entry.Value); // ディープコピー
            this.benchPlayerIds = benchPlayerIds.ToDictionary(entry => entry.Key, entry => entry.Value); // ディープコピー

            List<Position> unfieldablePositions = GetUnfieldablePositons();

            if (unfieldablePositions.Count() == 0) {
                // 与えられた変数の状況では、守備位置に問題がない場合
                // Console.WriteLine("解決しました");
                hasProblem = false;
                root.SendCompletedSignalToRootByEnd(this.history); // 子オブジェクトから親オブジェクトへ、走査完了（結果：補填方法有）の通知
            } else {
                // 与えられた変数の状況では、守備位置に問題がある場合
                hasProblem = true;
                foreach (Position unfieldablePosition in unfieldablePositions) {
                    if (history.process.Count > 4) continue; // ４階層以上は探索しない
                    List<ChangingOrderStep> compensateStepOptions = SearchCompensateStep(unfieldablePosition); // １つ先の複数未来を探索生成
                    // Console.WriteLine("問題" + unfieldablePosition + "に対して" + compensateStepOptions.Count.ToString() + "個の選択肢");
                    int i = 0;
                    foreach (ChangingOrderStep compensateOneStep in compensateStepOptions) {
                        // 複数の未来について各パターンごとに走査していく
                        i++;
                        // Console.WriteLine(i.ToString() + "個目 / " + compensateStepOptions.Count.ToString() + "[" + unfieldablePosition + "]");
                        ChangingOrderProcess newHistory = history.Clone();
                        newHistory.Add(compensateOneStep);
                        TeamStatus appliedTeamStatus = ApplyTeamStatus(new TeamStatus(this.order, this.positions, this.benchPlayerIds), compensateOneStep);
                        children.Add(new ChangingOrderProcessNode(root, players, player_map, newHistory, appliedTeamStatus.order, appliedTeamStatus.positions, appliedTeamStatus.benchPlayerIds));
                    }
                }
            }
        }

        public bool HasProblem()
        {
            return hasProblem;
        }

        // 守備位置の補正を行うことが可能かどうか
        public bool IsResoluble()
        {
            if (IsChild()) throw new Exception("child must not be called IsResoluble() function.");
            return hasProblem == false || hasProblem == true && changingOrderOptions.Count > 0;
        }

        // 子オブジェクトから親オブジェクトへ通知
        // historyには守備位置を補填するための手順が入っている
        // 「これで補填できる」と分かった最末端ノードの持つhistoryをそのままの形で最上部の親に１つずつ転送していく
        private void SendCompletedSignalToRootByEnd(ChangingOrderProcess history)
        {
            if (IsChild()) {
                root.SendCompletedSignalToRootByEnd(history);
            } else {
                changingOrderOptions.Add(history);
            }
        }

        // 選手を走査し不適切な守備位置のプレイヤーがいないか確認
        private List<Position> GetUnfieldablePositons()
        {
            List<Position> unfieldablePositions = new List<Position>();
            for (int i = 1; i <= 9; i++) {
                BattingOrder battingOrder = (BattingOrder)i;
                Position position = positions.FirstOrDefault(p => p.Value == battingOrder).Key;
                int player_id = order[battingOrder];
                Player player = player_map[player_id];
                // Console.Write(position + ":" + player.name + ',');
                if (position != Position.pitcher && position != Position.designated_hitter && !player.fieldablePositions.Contains(position)) {
                    unfieldablePositions.Add(position);
                }
            }

            return unfieldablePositions;
        }

        // 打てる手を探しそのリストを返す
        private List<ChangingOrderStep> SearchCompensateStep(Position necessaryPosition)
        {

            List<ChangingOrderStep> changingOrderSteps = new List<ChangingOrderStep>();
            BattingOrder compensatedPlayerOrder = positions[necessaryPosition];
            Player compensatedPlayer = player_map[order[compensatedPlayerOrder]]; // 守備位置が間違っている状態の選手

            // 出場中選手のサブポジションで代えられないか探す
            for (int i = 1; i <= 9; i++) {
                BattingOrder battingOrder = (BattingOrder)i;
                Position nowPosition = positions.FirstOrDefault(p => p.Value == battingOrder).Key; // playerの現在の守備位置
                if (battingOrder == compensatedPlayerOrder || nowPosition == Position.designated_hitter) continue;

                Player player = player_map[order[battingOrder]];
                List<Position> restPositions = player.fieldablePositions.Where(p => p != nowPosition).ToList(); // playerの交替可能な守備位置

                if (restPositions.Contains(necessaryPosition) && !history.IsDuplicate(compensatedPlayer.id, player.id)) {
                    // playerが守備位置交替により補完可能な場合
                    changingOrderSteps.Add(new ChangingOrderStep((int)necessaryPosition, (int)nowPosition, compensatedPlayer.id, player.id));
                }
            }

            // 控え選手との交替で探す
            foreach (KeyValuePair<int, BenchRole> pair in benchPlayerIds) {
                Player player = player_map[pair.Key];
                if (player.fieldablePositions.Contains(necessaryPosition) && !history.IsDuplicate(compensatedPlayer.id, player.id)) {
                    // playerとの選手交替により補完可能な場合
                    changingOrderSteps.Add(new ChangingOrderStep(null, null, compensatedPlayer.id, player.id));
                }
            }

            return changingOrderSteps;

        }

        private static TeamStatus ApplyTeamStatus(TeamStatus appliedTeamStatus, ChangingOrderStep changingOrderStep)
        {
            if (changingOrderStep.IsChangingFromBench()) {
                // ベンチの選手と交替
                BattingOrder changingBattingOrder = appliedTeamStatus.order.FirstOrDefault(pair => pair.Value == changingOrderStep.fromPlayerId).Key; // 選手交代がある打順
                appliedTeamStatus.order[changingBattingOrder] = changingOrderStep.toPlayerId; // 打順のところに新しい選手を入れる
                appliedTeamStatus.benchPlayerIds.Remove(changingOrderStep.toPlayerId); // ベンチからその選手を削除
            } else {
                // 守備位置の交替
                Position fromPosition = (Position)changingOrderStep.fromPosition.Value;
                Position toPosition = (Position)changingOrderStep.toPosition.Value;
                BattingOrder tmpBattingOrder = appliedTeamStatus.positions[fromPosition];
                appliedTeamStatus.positions[fromPosition] = appliedTeamStatus.positions[toPosition];
                appliedTeamStatus.positions[toPosition] = tmpBattingOrder;
            }
            return appliedTeamStatus;
        }

        private bool IsChild()
        {
            return isChild == true;
        }

        // commandを実行し、teamを上書き更新する
        public static void ExecuteCommand(Team team, ChangingOrderProcess command)
        {
            TeamStatus teamStatus = new TeamStatus(team.order, team.positions, team.benchPlayerIds);
            foreach (ChangingOrderStep step in command.process) {
                teamStatus = ApplyTeamStatus(teamStatus, step);
            }
            // teamを更新
            team.order = teamStatus.order;
            team.positions = teamStatus.positions;
            team.benchPlayerIds = teamStatus.benchPlayerIds;
        }
	}

    public struct ChangingOrderProcess
    {
        public List<ChangingOrderStep> process;
        public ChangingOrderProcess(List<ChangingOrderStep> process) {
            this.process = process;
        }

        public void Add(ChangingOrderStep changingOrderStep) {
            process.Add(changingOrderStep);
        }

        public ChangingOrderProcess Clone()
        {
            return new ChangingOrderProcess(this.process.Select(step => step.Clone()).ToList());
        }

        public bool IsDuplicate(int from_player_id, int to_player_id) {
            return process.Any(c => c.fromPlayerId == from_player_id && c.toPlayerId == to_player_id || c.fromPlayerId == to_player_id && c.toPlayerId == from_player_id);
        }

        // ベンチとの選手交代数を取得
        public int CountChangingFromBench()
        {
            return process.Where(step => step.IsChangingFromBench()).Count();
        }

        // 他守備位置との交代数を取得
        public int CountChangingFromField()
        {
            return process.Where(step => !step.IsChangingFromBench()).Count();
        }

    }

    public struct ChangingOrderStep
    {
        public int? fromPosition; // nullの場合ベンチの選手と交替という意味
        public int? toPosition; // nullの場合ベンチの選手と交替という意味
        public int fromPlayerId;
        public int toPlayerId;

        public ChangingOrderStep(int? fromPosition, int? toPosition, int fromPlayerId, int toPlayerId) {
            this.fromPosition = fromPosition;
            this.toPosition = toPosition;
            this.fromPlayerId = fromPlayerId;
            this.toPlayerId = toPlayerId;
        }

        public ChangingOrderStep Clone()
        {
            return new ChangingOrderStep(this.fromPosition, this.toPosition, this.fromPlayerId, this.toPlayerId);
        }

        public bool IsChangingFromBench()
        {
            return !(fromPosition.HasValue && toPosition.HasValue);
        }
    }

}

