﻿using System;
using System.Dynamic;
using System.Collections.Generic;

namespace BaseballSimulator
{
	public class PlayerBattingResult
	{
        public Dictionary<int, List<ResultPlay>> highrights = new Dictionary<int, List<ResultPlay>>();

        public int plate_appearance = 0;
        public int at_bats = 0;
        public int hits = 0;
        public int single = 0;
        public int twobase = 0;
        public int triple = 0;
        public int homerun = 0;
        public int rbi = 0;
        public int run_scored = 0;
        public int fourball = 0;
        public int strikeout = 0;
        public int groundout = 0;
        public int flyout = 0;
        public int sacrifice_bunt = 0;
        public int sacrifice_fly = 0;
        public int stolen_base = 0;
        public int caught_stealing = 0;
        public int doubleplay = 0;

	}
}

