﻿using System;
using System.Collections.Generic;

namespace BaseballSimulator
{
	public struct ResultPlay {

		public Position? position;
		public ResultType resultType;
        public int scored;
        Dictionary<Position, int> positionProbDistribution;
        static Random cRandom = new System.Random();

        // 10000が100%
        static Dictionary<ResultType, Dictionary<Position, int>> positionProbDistributionMap = new Dictionary<ResultType, Dictionary<Position, int>>() {
            { ResultType.single, new Dictionary<Position, int>{
                    { Position.pitcher, 30 }, { Position.catcher, 30 }, { Position.first, 170 }, { Position.second, 200 }, { Position.shortstop, 200 }, { Position.third, 170 }, { Position.left, 3500 }, { Position.center, 3300 }, { Position.right, 2400 }
            } },
            { ResultType.twobase, new Dictionary<Position, int>{
                    { Position.pitcher, 0 }, { Position.catcher, 0 }, { Position.first, 0 }, { Position.second, 0 }, { Position.shortstop, 0 }, { Position.third, 0 }, { Position.left, 3800 }, { Position.center, 2900 }, { Position.right, 3300 }
            } },
            { ResultType.triple, new Dictionary<Position, int>{
                    { Position.pitcher, 0 }, { Position.catcher, 0 }, { Position.first, 0 }, { Position.second, 0 }, { Position.shortstop, 0 }, { Position.third, 0 }, { Position.left, 3750 }, { Position.center, 2500 }, { Position.right, 3750 }
            } },
            { ResultType.homerun, new Dictionary<Position, int>{
                    { Position.pitcher, 0 }, { Position.catcher, 0 }, { Position.first, 0 }, { Position.second, 0 }, { Position.shortstop, 0 }, { Position.third, 0 }, { Position.left, 6500 }, { Position.center, 2000 }, { Position.right, 1500 }
            } },
            { ResultType.fourball, null },
            { ResultType.strikeout, null },
            { ResultType.groundout, new Dictionary<Position, int>{
                    { Position.pitcher, 970 }, { Position.catcher, 30 }, { Position.first, 1800 }, { Position.second, 2700 }, { Position.shortstop, 2700 }, { Position.third, 1800 }, { Position.left, 0 }, { Position.center, 0 }, { Position.right, 0 }
            } },
            { ResultType.flyout, new Dictionary<Position, int>{
                    { Position.pitcher, 100 }, { Position.catcher, 300 }, { Position.first, 300 }, { Position.second, 400 }, { Position.shortstop, 400 }, { Position.third, 300 }, { Position.left, 2900 }, { Position.center, 2700 }, { Position.right, 2600 }
            } },
            { ResultType.sacrifice_bunt, new Dictionary<Position, int>{
                    { Position.pitcher, 3500 }, { Position.catcher, 1500 }, { Position.first, 2500 }, { Position.second, 0 }, { Position.shortstop, 0 }, { Position.third, 2500 }, { Position.left, 0 }, { Position.center, 0 }, { Position.right, 0 }
            } },
            { ResultType.sacrifice_fly, new Dictionary<Position, int>{
                    { Position.pitcher, 0 }, { Position.catcher, 0 }, { Position.first, 0 }, { Position.second, 0 }, { Position.shortstop, 0 }, { Position.third, 0 }, { Position.left, 3500 }, { Position.center, 3300 }, { Position.right, 3200 }
            } },
            { ResultType.doubleplay, new Dictionary<Position, int>{
                    { Position.pitcher, 1300 }, { Position.catcher, 0 }, { Position.first, 1300 }, { Position.second, 3000 }, { Position.shortstop, 3000 }, { Position.third, 1400 }, { Position.left, 0 }, { Position.center, 0 }, { Position.right, 0 }
            } },
            { ResultType.foulfly, new Dictionary<Position, int>{
                    { Position.pitcher, 0 }, { Position.catcher, 2000 }, { Position.first, 2000 }, { Position.second, 1000 }, { Position.shortstop, 1000 }, { Position.third, 2000 }, { Position.left, 1000 }, { Position.center, 0 }, { Position.right, 1000 }
            } },
        };

        public ResultPlay(ResultType resultType)
        {
            this.resultType = resultType;
            this.position = null;
            this.scored = 0;
            positionProbDistribution = positionProbDistributionMap[resultType];
        }

        public void ChangeProbDistribution (Position fromPosition, Position toPosition, int amount)
        {
            positionProbDistribution[fromPosition] -= amount;
            positionProbDistribution[toPosition] += amount;
            if (positionProbDistribution[fromPosition] < 0 || positionProbDistribution[fromPosition] > 10000) {
                throw new Exception("確率が0%~100%の範囲におさまっていません");
            }
        }

        public void DecidePosition()
        {
            if (positionProbDistribution == null) {
                // strikeoutやfourballのとき
                return;
            }

            RandomGacha<Position> randomGacha = new RandomGacha<Position>(10000);
            foreach (KeyValuePair<Position, int> pair in positionProbDistribution) {
                if (pair.Value > 0) {
                    randomGacha.Add(pair.Value, pair.Key);
                }
            }
            position = randomGacha.Next();
        }

        public void SetScored(int scored)
        {
            this.scored = scored;
        }

    }
}
