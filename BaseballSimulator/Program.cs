﻿using System;

namespace BaseballSimulator
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			GameProcessor gameProcessor = new GameProcessor();
            if (args.Length > 0) {
                if (args[0] == "test") {
                    gameProcessor.TestRun();
                } else if (args[0] == "gettestjson") {
                    gameProcessor.GetTestJson();
                } else {
                    gameProcessor.Run(args[0]);
                }
            }

		}
	}
}
