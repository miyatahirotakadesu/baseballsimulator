﻿using System;
using System.Collections.Generic;

namespace BaseballSimulator
{
    // 采配のための試合状況
	public struct GameStatus
	{

        public int inning;
        public InningStatus inningStatus;
        public Dictionary<ODType, TeamType> teamTypeMap;
        public int outcount;
        public RunnerStatus runnerStatus;
        public int scoreDiff; // 攻撃中チームのリードスコア
        public int offenceTeamScore;
        public Dictionary<TeamType, Dictionary<int, int>> scoreHighLight;

        public GameStatus(int inning, InningStatus inningStatus, Dictionary<ODType, TeamType> teamTypeMap, int outcount, RunnerStatus runnerStatus, int scoreDiff, int offenceTeamScore, Dictionary<TeamType, Dictionary<int, int>> scoreHighLight)
        {
            this.inning = inning;
            this.inningStatus = inningStatus;
            this.teamTypeMap = teamTypeMap;
            this.outcount = outcount;
            this.runnerStatus = runnerStatus;
            this.scoreDiff = scoreDiff; // 攻撃中チームのリードスコア
            this.offenceTeamScore = offenceTeamScore;
            this.scoreHighLight = scoreHighLight;
        }

        // ランナーの数を返す
        public int GetRunnerCount()
        {
            int runnerCount;
            switch (runnerStatus) {
                case RunnerStatus.first:
                case RunnerStatus.second:
                case RunnerStatus.third:
                    runnerCount = 1;
                    break;
                case RunnerStatus.first_second:
                case RunnerStatus.third_first:
                case RunnerStatus.third_second:
                    runnerCount = 2;
                    break;
                case RunnerStatus.fullbase:
                    runnerCount = 3;
                    break;
                default:
                    runnerCount = 0;
                    break;

            }
            return runnerCount;
        }

        // 0~100で場面の重要度を返す
        public int CalcImportance()
        {
            int proInning = inning;
            if (proInning > 9) proInning = 9;
            proInning -= 5; // -5~4;
            if (proInning < 0) proInning = 0; // 0~4
            int inningImportance = 6 * proInning; // 0~24

            int runnerImportance = 2 * (int)runnerStatus; // 0~14

            int proScoreDiff = Math.Abs(scoreDiff);
            if (scoreDiff > 0) proScoreDiff *= 2; // リード時は重要性を低く考える
            int scoreDiffImportance = (8 - proScoreDiff) * 4 - 2; // -∞~30

            int proRunnerScoreImportance = scoreDiff + GetRunnerCount();// 塁上のランナーが全員帰ったらN点差になるという値
            int runnerScoreImportance = (8 - proRunnerScoreImportance) * 4; // -∞~32

            int importance = inningImportance + runnerImportance + scoreDiffImportance + runnerScoreImportance;
            return importance;

        }

        public bool IsInitialOfInning()
        {
            // イニングの開始直後かどうか
            TeamType defenceTeamType = teamTypeMap[ODType.defence];
            return outcount == 0 && GetRunnerCount() == 0 && scoreHighLight[defenceTeamType].ContainsKey(inning) == false;
        }

        public bool IsRelieverSituation()
        {
            // ７回以降同点または３点以内のリードであり
            // 良いリリーフ投手が登板するにふさわしい場面かどうか
            return IsInitialOfInning() == true && IsCloserSituation() == false && inning >= 7 && scoreDiff <= 0 && scoreDiff >= -3;
        }

        public bool IsCloserSituation()
        {
            // ３点以内のリードで抑えれば勝利といった、抑え投手が登板するにふさわしい場面かどうか
            bool cond1 = IsInitialOfInning() == true && inning >= 9 && (scoreDiff < 0 && scoreDiff >= -3 || scoreDiff == 0 && inningStatus == InningStatus.top);
            bool cond2 = inning >= 9 && GetRunnerCount() >= 2 && scoreDiff <= -4 && scoreDiff + GetRunnerCount() + 2 >= 0;
            return cond1 || cond2;
        }

	}

}

