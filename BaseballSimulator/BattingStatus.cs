﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace BaseballSimulator
{
	public struct BattingStatus
	{

		public int teams_game;
		public int game;
		public int plate_appearance;
		public int hits;
		public int single;
		public int twobase;
		public int triple;
		public int homerun;
		public int rbi;
		public int fourball;
		public int deadball;
		public int strikeout;
		public int sacrifice_bunt;
		public int sacrifice_fly;
		public int stolen_base;
		public int caught_stealing;
		public int doubleplay;

		public int at_bat;
		public int total_bases;
		public float obp;
		public float slg;
		public float ops;

        public bool is_pitcher;

        public Dictionary<ResultType, int> resultProbDistribution;
        static readonly int SUM_PROB = 5000;
        static readonly float GO_FO = 1.08f; // ゴロアウト / フライアウト 比率

		public void SetStatusInBulk(int teams_game, int game, int plate_appearance, int hits, int twobase, int triple, int homerun, int rbi, int fourball, int strikeout, int sacrifice_bunt, int sacrifice_fly, int stolen_base, int caught_stealing, int doubleplay) {
            this.teams_game = teams_game;
			this.game = game;
			this.plate_appearance = plate_appearance;
			this.hits = hits;
			this.twobase = twobase;
			this.triple = triple;
			this.homerun = homerun;
			this.rbi = rbi;
			this.fourball = fourball;
			this.strikeout = strikeout;
			this.sacrifice_bunt = sacrifice_bunt;
			this.sacrifice_fly = sacrifice_fly;
			this.stolen_base = stolen_base;
			this.caught_stealing = caught_stealing;
			this.doubleplay = doubleplay;
		}

        [OnDeserialized]
        private void OnDeserialized(StreamingContext context)
        {
            MakeUpPAShortage();
            CalcPlayerProbDistribution();
            SetRelatedVars();
        }

        void MakeUpPAShortage()
        {
            // 不足打席を代替レベルで補う
            int necessary_plate_appearance = Convert.ToInt32((float)teams_game * 3.1f);

            if (plate_appearance < necessary_plate_appearance) {
                BattingStatus repStatus = (is_pitcher == true) ? GetPitcherLevelStatus() : GetReplacementLevelStatus();
                float shortage_magnification = (float)(necessary_plate_appearance - plate_appearance) / 100.0f; // 100打席足りないとしたら1.0fになる
                plate_appearance += Convert.ToInt32((float)repStatus.plate_appearance * shortage_magnification);
                hits += Convert.ToInt32((float)repStatus.hits * shortage_magnification);
                twobase += Convert.ToInt32((float)repStatus.twobase * shortage_magnification);
                triple += Convert.ToInt32((float)repStatus.triple * shortage_magnification);
                homerun += Convert.ToInt32((float)repStatus.homerun * shortage_magnification);
                fourball += Convert.ToInt32((float)repStatus.fourball * shortage_magnification);
                strikeout += Convert.ToInt32((float)repStatus.strikeout * shortage_magnification);
                sacrifice_bunt += Convert.ToInt32((float)repStatus.sacrifice_bunt * shortage_magnification);
                sacrifice_fly += Convert.ToInt32((float)repStatus.sacrifice_fly * shortage_magnification);
                stolen_base += Convert.ToInt32((float)repStatus.stolen_base * shortage_magnification);
                caught_stealing += Convert.ToInt32((float)repStatus.caught_stealing * shortage_magnification);
                doubleplay += Convert.ToInt32((float)repStatus.doubleplay * shortage_magnification);
            }
        }

        void CalcPlayerProbDistribution()
        {
            single = hits - twobase - triple - homerun;
            int probSingle = SUM_PROB * single / plate_appearance;
            int probTwobase = SUM_PROB * twobase /  plate_appearance;
            int probTriple = SUM_PROB * triple / plate_appearance;
            int probHomerun = SUM_PROB * homerun / plate_appearance;
            int probStrikeout = SUM_PROB * strikeout / plate_appearance;
            int probFourball = SUM_PROB * fourball / plate_appearance;

            int restProb = SUM_PROB - probSingle - probTwobase - probTriple - probHomerun - probStrikeout - probFourball;
            int probGroundout = Convert.ToInt32((float)restProb * (GO_FO / (1.0f + GO_FO)));
            int probFlyout = restProb - probGroundout;

            SetPlayerProbDistribution(probSingle, probTwobase, probTriple, probHomerun, probStrikeout, probFourball, probGroundout, probFlyout);
        }

        void SetPlayerProbDistribution (int single, int twobase, int triple, int homerun, int strikeout, int fourball, int groundout, int flyout)
        {
            if (single + twobase + triple + homerun + strikeout + fourball + groundout + flyout != SUM_PROB) {
                throw new Exception("確率分布の数値合計が" + SUM_PROB + "になるようにしてください");
            }

            resultProbDistribution = new Dictionary<ResultType, int>();
            resultProbDistribution.Add(ResultType.single, single);
            resultProbDistribution.Add(ResultType.twobase, twobase);
            resultProbDistribution.Add(ResultType.triple, triple);
            resultProbDistribution.Add(ResultType.homerun, homerun);
            resultProbDistribution.Add(ResultType.strikeout, strikeout);
            resultProbDistribution.Add(ResultType.fourball, fourball);
            resultProbDistribution.Add(ResultType.groundout, groundout);
            resultProbDistribution.Add(ResultType.flyout, flyout);
        }

        // OPSなどを自動計算設定
        void SetRelatedVars()
        {
            at_bat = plate_appearance - fourball - sacrifice_bunt - sacrifice_fly;
            total_bases = single + twobase * 2 + triple * 3 + homerun * 4;
            obp = (float)Math.Round((float)(hits + fourball) / (float)plate_appearance, 3, MidpointRounding.AwayFromZero);
            slg = (float)Math.Round((float)total_bases / (float)at_bat, 3, MidpointRounding.AwayFromZero);
            ops = obp + slg;
        }

        private static BattingStatus GetReplacementLevelStatus()
        {
            BattingStatus battingStatus = new BattingStatus();
		    battingStatus.plate_appearance = 100;
		    battingStatus.hits = 20;
		    battingStatus.twobase = 4;
		    battingStatus.triple = 1;
		    battingStatus.homerun = 1;
		    battingStatus.fourball = 8;
		    battingStatus.strikeout = 28;
		    battingStatus.sacrifice_bunt = 4;
		    battingStatus.sacrifice_fly = 1;
		    battingStatus.stolen_base = 0;
		    battingStatus.caught_stealing = 0;
		    battingStatus.doubleplay = 2;
            return battingStatus;
        }

        private static BattingStatus GetPitcherLevelStatus()
        {
            BattingStatus battingStatus = new BattingStatus();
		    battingStatus.plate_appearance = 100;
		    battingStatus.hits = 4;
		    battingStatus.twobase = 1;
		    battingStatus.triple = 0;
		    battingStatus.homerun = 0;
		    battingStatus.fourball = 2;
		    battingStatus.strikeout = 35;
		    battingStatus.sacrifice_bunt = 15;
		    battingStatus.sacrifice_fly = 1;
		    battingStatus.stolen_base = 0;
		    battingStatus.caught_stealing = 0;
		    battingStatus.doubleplay = 2;
            return battingStatus;
        }

        public static BattingStatus GetBattingStatusForReplacement()
        {
            BattingStatus battingStatus = GetReplacementLevelStatus();
            battingStatus.CalcPlayerProbDistribution();
            battingStatus.SetRelatedVars();
            return battingStatus;
        }

        public static BattingStatus GetBattingStatusForPitcher()
        {
            BattingStatus battingStatus = GetPitcherLevelStatus();
            battingStatus.CalcPlayerProbDistribution();
            battingStatus.SetRelatedVars();
            return battingStatus;
        }

	}
}

