﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace BaseballSimulator
{

	public struct PitchingStatus
	{
		public int teams_game;
		public int game;
		public int game_starter;
		public int plate_appearance;
		public int getout;
		public int allowed_runs;
		public int earned_runs;
		public int allowed_hits;
		public int allowed_homeruns;
		public int strikeouts;
		public int bases_on_balls;
		public int hit_by_pitches;
		public int wins;
		public int losses;
		public int complete_games;
		public int shutouts;
		public int holds;
		public int saves;

        public int average_plate_appearance; // 投手が平均どのぐらいイニング投げれるか判定のため利用
		public int estimated_allowed_twobases;
		public int estimated_allowed_triples;
        public Dictionary<ResultType, int> resultProbDistribution;
        static readonly int SUM_PROB = 5000;
        static readonly float GO_FO = 1.08f; // ゴロアウト / フライアウト 比率

        readonly static Dictionary<ResultType, float> longhit_criteria = new Dictionary<ResultType, float>() {
            { ResultType.single, 0.712f }, { ResultType.twobase, 0.171f }, { ResultType.triple, 0.015f }, { ResultType.homerun, 0.102f }
        };

		public void SetStatusInBulk(int teams_game, int game, int game_starter, int plate_appearance, int getout, int allowed_runs, int earned_runs, int allowed_hits, int allowed_homeruns, int strikeouts, int bases_on_balls, int wins, int losses, int complete_games, int shutouts, int holds, int saves) {
			this.teams_game = teams_game;
			this.game = game;
			this.game_starter = game_starter;
			this.plate_appearance = plate_appearance;
			this.getout = getout;
			this.allowed_runs = allowed_runs;
			this.earned_runs = earned_runs;
			this.allowed_hits = allowed_hits;
			this.allowed_homeruns = allowed_homeruns;
			this.strikeouts = strikeouts;
			this.bases_on_balls = bases_on_balls;
			this.wins = wins;
			this.losses = losses;
			this.complete_games = complete_games;
			this.shutouts = shutouts;
			this.holds = holds;
			this.saves = saves;
		}

        [OnDeserialized]
        private void OnDeserialized(StreamingContext context)
        {
            MakeUpPAShortage();
            CalcPlayerProbDistribution();
        }

        void MakeUpPAShortage()
        {
            // 不足打席を代替レベルで補う
            bool is_starter = game < game_starter * 2;
            int necessary_plate_appearance = (is_starter) ? teams_game * 3 : teams_game; // 先発なら規定イニング = 試合数、中継ぎなら(試合数/3)イニング

            if (plate_appearance < necessary_plate_appearance) {
                PitchingStatus repStatus = GetReplacementLevelStatus(is_starter);
                float shortage_magnification = (float)(necessary_plate_appearance - plate_appearance) / 1000.0f; // 1000打席(対戦打者数)足りないとしたら1.0fになる
                plate_appearance += Convert.ToInt32((float)repStatus.plate_appearance * shortage_magnification);
                getout += Convert.ToInt32((float)repStatus.getout * shortage_magnification);
                allowed_hits += Convert.ToInt32((float)repStatus.allowed_hits * shortage_magnification);
                allowed_homeruns += Convert.ToInt32((float)repStatus.allowed_homeruns * shortage_magnification);
                bases_on_balls += Convert.ToInt32((float)repStatus.bases_on_balls * shortage_magnification);
                strikeouts += Convert.ToInt32((float)repStatus.strikeouts * shortage_magnification);
            }
        }

        void CalcPlayerProbDistribution()
        {
            average_plate_appearance = Convert.ToInt32((float)plate_appearance / (float)game);

            float longhit_rate = ((float)allowed_homeruns / (float)allowed_hits) / longhit_criteria[ResultType.homerun]; // 標準であれば1.0f
            estimated_allowed_twobases = Convert.ToInt32((float)allowed_hits * longhit_criteria[ResultType.twobase] * longhit_rate);
            estimated_allowed_triples = Convert.ToInt32((float)allowed_hits * longhit_criteria[ResultType.triple] * longhit_rate);

            int probSingle = SUM_PROB * (allowed_hits - estimated_allowed_twobases - estimated_allowed_triples - allowed_homeruns) / plate_appearance;
            int probTwobase = SUM_PROB * estimated_allowed_twobases / plate_appearance;
            int probTriple = SUM_PROB * estimated_allowed_triples / plate_appearance;
            int probHomerun = SUM_PROB * allowed_homeruns / plate_appearance;
            int probStrikeout = SUM_PROB * strikeouts / plate_appearance;
            int probFourball = SUM_PROB * bases_on_balls / plate_appearance;

            int restProb = SUM_PROB - probSingle - probTwobase - probTriple - probHomerun - probStrikeout - probFourball;
            int probGroundout = Convert.ToInt32((float)restProb * (GO_FO / (1.0f + GO_FO)));
            int probFlyout = restProb - probGroundout;

            SetPlayerProbDistribution(probSingle, probTwobase, probTriple, probHomerun, probStrikeout, probFourball, probGroundout, probFlyout);
        }

        void SetPlayerProbDistribution (int single, int twobase, int triple, int homerun, int strikeout, int fourball, int groundout, int flyout)
        {
            if (single + twobase + triple + homerun + strikeout + fourball + groundout + flyout != SUM_PROB) {
                throw new Exception("確率分布の数値合計が" + SUM_PROB + "になるようにしてください");
            }

            resultProbDistribution = new Dictionary<ResultType, int>();
            resultProbDistribution.Add(ResultType.single, single);
            resultProbDistribution.Add(ResultType.twobase, twobase);
            resultProbDistribution.Add(ResultType.triple, triple);
            resultProbDistribution.Add(ResultType.homerun, homerun);
            resultProbDistribution.Add(ResultType.strikeout, strikeout);
            resultProbDistribution.Add(ResultType.fourball, fourball);
            resultProbDistribution.Add(ResultType.groundout, groundout);
            resultProbDistribution.Add(ResultType.flyout, flyout);
        }

        private static PitchingStatus GetReplacementLevelStatus(bool isStarter)
        {
            PitchingStatus pitchingStatus = new PitchingStatus();
            pitchingStatus.game = 36;
            pitchingStatus.plate_appearance = 1000;
            pitchingStatus.getout = 540;
            if (isStarter) {
                pitchingStatus.allowed_homeruns = 35;
                pitchingStatus.bases_on_balls = 100;
                pitchingStatus.strikeouts = 120;
            } else {
                pitchingStatus.allowed_homeruns = 32;
                pitchingStatus.bases_on_balls = 100;
                pitchingStatus.strikeouts = 130;
            }
            pitchingStatus.allowed_hits = Convert.ToInt32((float)pitchingStatus.allowed_homeruns / longhit_criteria[ResultType.homerun]);
            return pitchingStatus;
        }

        public static PitchingStatus GetPitchingStatusForReplacement(bool isStarter)
        {
            PitchingStatus pitchingStatus = GetReplacementLevelStatus(isStarter);
            pitchingStatus.CalcPlayerProbDistribution();
            return pitchingStatus;
        }


	}
}

