﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace BaseballSimulator
{
	[JsonObject("team")]
	public class Team {

		[JsonProperty("id")]
		public string id;
		[JsonProperty("order")]
		public Dictionary<BattingOrder, int> order;
		[JsonProperty("positions")]
		public Dictionary<Position, BattingOrder> positions;
		[JsonProperty("bench_player_ids")]
		public Dictionary<int, BenchRole> benchPlayerIds;
		[JsonProperty("players")]
		public List<Player> players;

        Dictionary<int, Player> player_map; // <Player.id, Player>
        BattingOrder battingOrder = BattingOrder.order1;

        [OnDeserialized]
        private void OnDeserialized(StreamingContext context)
        {
            player_map = new Dictionary<int, Player>();
            foreach (Player player in players) {
                // スタメン選手にはフラグを立てる
                bool IsStarter = order.Any(pair => pair.Value == player.id);
                if (IsStarter) {
                    player.SetStarterRole();
                }

                player_map.Add(player.id, player);
            }
            foreach (KeyValuePair<BattingOrder, int> pair in order) {
                players.First(player => player.id == pair.Value).ParticipateGame(1, pair.Key);
            }
        }

        public static Team MakeTestTeam()
        {
			BattingStatus battingStatus = new BattingStatus();
			battingStatus.SetStatusInBulk(100, 130, 600, 150, 20, 3, 10, 70, 20, 100, 5, 5, 5, 2, 5);
			PitchingStatus pitchingStatus = new PitchingStatus();
			pitchingStatus.SetStatusInBulk(100, 30, 27, 600, 480, 100, 100, 150, 10, 100, 40, 10, 8, 1, 1, 0, 0);

            Dictionary<BattingOrder, int> order = new Dictionary<BattingOrder, int>() {
                { BattingOrder.order1, 1 }, { BattingOrder.order2, 2 }, { BattingOrder.order3, 3 }, { BattingOrder.order4, 4 }, { BattingOrder.order5, 5 }, { BattingOrder.order6, 6 }, { BattingOrder.order7, 7 }, { BattingOrder.order8, 8 }, { BattingOrder.order9, 17 }
            };
            Dictionary<Position, BattingOrder> positions = new Dictionary<Position, BattingOrder>() {
                { Position.center, BattingOrder.order1 }, { Position.second, BattingOrder.order2 }, { Position.left, BattingOrder.order3 }, { Position.third, BattingOrder.order4 }, { Position.first, BattingOrder.order5 }, { Position.right, BattingOrder.order6 }, { Position.shortstop, BattingOrder.order7 }, { Position.catcher, BattingOrder.order8 }, { Position.pitcher, BattingOrder.order9 }
            };
            Dictionary<int, BenchRole> benchPlayerIds = new Dictionary<int, BenchRole>() {
                { 9, BenchRole.dependable_pinch_hitter }, { 10, BenchRole.second_pinch_hitter }, { 11, BenchRole.utility }, { 12, BenchRole.fielder }, { 13, BenchRole.utility }, { 14, BenchRole.third_pinch_hitter }, { 15, BenchRole.dependable_pinch_hitter }, { 16, BenchRole.second_pinch_hitter }, { 18, BenchRole.long_relief }, { 19, BenchRole.short_relief }, { 20, BenchRole.pre_setup_pitcher }, { 21, BenchRole.setup_pitcher }, { 22, BenchRole.short_relief }, { 23, BenchRole.long_relief }, { 24, BenchRole.setup_pitcher }, { 25, BenchRole.closer }
            };
            List<Player> players = new List<Player>();

            // player
            for (int i = 1; i <= 16; i++) {
                List<Position> fieldablePositions = new List<Position>();
                Position position;
                if (i == 1) {
                    position = Position.first;
                } else if (i == 2) {
                    fieldablePositions.Add(Position.center);
                    position = positions.FirstOrDefault(p => p.Value == (BattingOrder)i).Key;
                } else if (i == 3) {
                    position = Position.shortstop;
                } else if (i == 4) {
                    position = Position.catcher;
                } else if (i == 7) {
                    position = Position.right;
                } else if (i == 9) {
                    position = Position.first;
                } else if (i == 11) {
                    fieldablePositions.Add(Position.second);
                    position = Position.center;
                } else if (i == 12) {
                    position = Position.second;
                } else if (i == 13) {
                    position = Position.third;
                } else if (i == 14) {
                    position = Position.shortstop;
                } else if (i == 15) {
                    position = Position.left;
                } else if (i == 16) {
                    position = Position.catcher;
                } else if (i == 17) {
                    position = Position.right;
                } else {
                    position = positions.FirstOrDefault(p => p.Value == (BattingOrder)i).Key;
                }
                fieldablePositions.Add(position);
                Player player = new Player(i, "テストBatter" + i.ToString(), battingStatus, null, fieldablePositions);
                players.Add(player);
            }
            for (int i = 17; i <= 25; i++) {
                List<Position> fieldablePositions = new List<Position>();
                fieldablePositions.Add(Position.pitcher);
                Player player = new Player(i, "テストPitcher" + i.ToString(), null, pitchingStatus, fieldablePositions);
                players.Add(player);
            }

            Team team = new Team();
            team.id = "123";
            team.order = order;
            team.positions = positions;
            team.benchPlayerIds = benchPlayerIds;
            team.players = players;

            return team;
        }

        // 守備位置が適正かどうか確認し、変更が必要であれば変更を行う。返り値はその変更の内容。
        public List<ChangingOrderStep> CheckPosition(GameStatus gameStatus)
        {
            ChangingOrderProcessNode parentNode = new ChangingOrderProcessNode(players, player_map, new ChangingOrderProcess(new List<ChangingOrderStep>()), order, positions, benchPlayerIds);
            //Console.WriteLine(JsonConvert.SerializeObject(parentNode.changingOrderOptions, Formatting.Indented));
            if (parentNode.changingOrderOptions.Count > 0) {
                int minChangingFromBenchCount = parentNode.changingOrderOptions.Min(option => option.CountChangingFromBench());
                int minChangingFromFieldCount = parentNode.changingOrderOptions.Where(option => option.CountChangingFromBench() == minChangingFromBenchCount).Min(option => option.CountChangingFromField());
                 // (入力)オーダーとベンチとSteps→(出力)次の候補Step一覧を返す or 次の候補Step一覧が０件 or 守備位置補正の完了連絡
                 // 同じStepは行わない、次のStepが見つけられなくなったらそのProcessは消滅
                ChangingOrderProcess command = parentNode.changingOrderOptions
                                               .Where(option => option.CountChangingFromBench() == minChangingFromBenchCount)
                                               .Where(option => option.CountChangingFromField() == minChangingFromFieldCount)
                                               .First();
                // 守備位置の変更実行
                ChangingOrderProcessNode.ExecuteCommand(this, command);

                // 守備位置変更により出場した選手に対して出場登録
                foreach (ChangingOrderStep step in command.process) {
                    if (step.IsChangingFromBench()) {
                        BattingOrder batting_order = order.First(order_item => order_item.Value == step.toPlayerId).Key;
                        players.First(player => player.id == step.toPlayerId).ParticipateGame(gameStatus.inning, batting_order);
                    }
                }
                
                // この関数内で行った守備変更内容を返す
                return command.process;
            } else {
                // 特に何もしなかったということを返す
                return new List<ChangingOrderStep>();
            }
        }

        public ChangingPlayer? DecideReliefPitcher(GameStatus gameStatus)
        {
            int? decidedReliefPitcher = null;
            Player pitcher = GetPitcher();

            if (!pitcher.fieldablePositions.Contains(Position.pitcher)) {
                // 投手に代打が出されていた場合
                if (gameStatus.inning <= 6) {
                    decidedReliefPitcher = FindPlayerIDByRole(BenchRole.long_relief);
                } else if (gameStatus.inning == 7) {
                    decidedReliefPitcher = (gameStatus.IsRelieverSituation()) ?  FindPlayerIDByRole(BenchRole.pre_setup_pitcher) : FindPlayerIDByRole(BenchRole.long_relief);
                } else if (gameStatus.inning == 8) {
                    decidedReliefPitcher = (gameStatus.IsRelieverSituation()) ?  FindPlayerIDByRoles(new List<BenchRole> { BenchRole.setup_pitcher, BenchRole.pre_setup_pitcher }) : FindPlayerIDByRole(BenchRole.long_relief);
                } else if (gameStatus.inning >= 9) {
                    if (gameStatus.IsCloserSituation()) {
                        // 一番いい中継ぎ投手を出す場面
                        decidedReliefPitcher = FindPlayerIDByRoles(new List<BenchRole> { BenchRole.closer, BenchRole.setup_pitcher, BenchRole.pre_setup_pitcher });
                    } else {
                        decidedReliefPitcher = (gameStatus.scoreDiff < -6 /*大量リード時*/ ) ? FindPlayerIDByRoles(new List<BenchRole> { BenchRole.pre_setup_pitcher, BenchRole.setup_pitcher, BenchRole.closer }) : FindPlayerIDByRoles(new List<BenchRole> { BenchRole.closer, BenchRole.setup_pitcher, BenchRole.pre_setup_pitcher });
                    }
                }
            } else if (pitcher.IsStarter()) {
                // マウンドの投手が先発であれば、先発が限界かどうかによって判定する
                // イニング、スタミナ、ランナー数、失点数により限界かどうかを判断
                int decideVal = 0;

                // イニング
                if (gameStatus.inning <= 3) {
                    decideVal += 0;
                } else if (gameStatus.inning <= 5) {
                    decideVal += 30;
                } else if (gameStatus.inning == 6) {
                    decideVal += 40;
                } else if (gameStatus.inning == 7) {
                    decideVal += 45;
                } else if (gameStatus.inning == 8) {
                    decideVal += 50;
                } else if (gameStatus.inning == 9) {
                    decideVal += 60;
                } else {
                    decideVal += 70;
                }

                // スタミナ
                decideVal += 3 * (gameStatus.inning - 1) + gameStatus.outcount - pitcher.pitchingStatus.Value.average_plate_appearance;

                // ランナー数
                decideVal += gameStatus.GetRunnerCount() * 5;

                // 失点数
                decideVal += pitcher.pitchingResult.allowed_runs * 10;

                // 勝利の方程式を作りたい
                if (gameStatus.IsCloserSituation()) {
                    decideVal += 25;
                } else if (gameStatus.IsRelieverSituation()) {
                    decideVal += 15;
                }

                if (decideVal > 100) {
                    // 先発投手交代
                    if (gameStatus.inning <= 6) {
                        decidedReliefPitcher = FindPlayerIDByRole(BenchRole.long_relief);
                    } else if (gameStatus.inning == 7) {
                        decidedReliefPitcher = (gameStatus.IsRelieverSituation()) ?  FindPlayerIDByRole(BenchRole.pre_setup_pitcher) : FindPlayerIDByRole(BenchRole.long_relief);
                    } else if (gameStatus.inning == 8) {
                        decidedReliefPitcher = (gameStatus.IsRelieverSituation()) ?  FindPlayerIDByRoles(new List<BenchRole> { BenchRole.setup_pitcher, BenchRole.pre_setup_pitcher }) : FindPlayerIDByRole(BenchRole.long_relief);
                    } else if (gameStatus.inning >= 9) {
                        if (gameStatus.IsCloserSituation()) {
                            // 一番いい中継ぎ投手を出す場面
                            decidedReliefPitcher = FindPlayerIDByRoles(new List<BenchRole> { BenchRole.closer, BenchRole.setup_pitcher, BenchRole.pre_setup_pitcher });
                        } else {
                            decidedReliefPitcher = (gameStatus.scoreDiff < -6 /*大量リード時*/ ) ? FindPlayerIDByRoles(new List<BenchRole> { BenchRole.pre_setup_pitcher, BenchRole.setup_pitcher, BenchRole.closer }) : FindPlayerIDByRoles(new List<BenchRole> { BenchRole.closer, BenchRole.setup_pitcher, BenchRole.pre_setup_pitcher });
                        }
                    }
                }

            } else {
                // マウンドの投手が中継ぎであれば、GameStatusにふさわしい投手がいるかどうかで判定する
                if (gameStatus.IsCloserSituation()) {
                    decidedReliefPitcher = FindPlayerIDByRoles(new List<BenchRole> { BenchRole.closer, BenchRole.setup_pitcher, BenchRole.pre_setup_pitcher });
                } else if (gameStatus.IsRelieverSituation()) {
                    decidedReliefPitcher = (gameStatus.inning == 7) ? FindPlayerIDByRole(BenchRole.pre_setup_pitcher) : FindPlayerIDByRole(BenchRole.setup_pitcher);
                } else if (gameStatus.GetRunnerCount() + pitcher.pitchingResult.allowed_hits - pitcher.pitchingResult.getout / 3 >= 7) {
                    // 中継ぎ投手の調子が悪そうな場合
                    decidedReliefPitcher = FindPlayerIDByRoles(new List<BenchRole> { BenchRole.short_relief, BenchRole.normal_relief, BenchRole.long_relief, BenchRole.pre_setup_pitcher });
                }
            }


            ChangingPlayer? changingPlayer = null;
            if (decidedReliefPitcher.HasValue) {
                // 投手交代の実行
                BattingOrder pitcherBattingOrder = positions[Position.pitcher];
                changingPlayer = new ChangingPlayer(order[pitcherBattingOrder], decidedReliefPitcher.Value);
                order[pitcherBattingOrder] = decidedReliefPitcher.Value;
                players.First(player => player.id == decidedReliefPitcher.Value).ParticipateGame(gameStatus.inning, pitcherBattingOrder);
                benchPlayerIds.Remove(decidedReliefPitcher.Value);
            }

            return changingPlayer;
        }

        //{ new PitcherStatForPinchHitter(イニング, 失点), 投手に代打を出すインセンティブ }
        private static Dictionary<PitcherStatForPinchHitter, int> pinchHitterToPitcherIncentives = new Dictionary<PitcherStatForPinchHitter, int>() {
            { new PitcherStatForPinchHitter(0, 0), 0 }, { new PitcherStatForPinchHitter(0, 1), 0 }, { new PitcherStatForPinchHitter(0, 2), 0 }, { new PitcherStatForPinchHitter(0, 3), 0 }, { new PitcherStatForPinchHitter(0, 4), 10 }, { new PitcherStatForPinchHitter(0, 5), 10 }, { new PitcherStatForPinchHitter(0, 6), 20 }, { new PitcherStatForPinchHitter(0, 7), 20 }, { new PitcherStatForPinchHitter(0, 8), 30 }, { new PitcherStatForPinchHitter(0, 9), 30 },
            { new PitcherStatForPinchHitter(1, 0), 0 }, { new PitcherStatForPinchHitter(1, 1), 0 }, { new PitcherStatForPinchHitter(1, 2), 0 }, { new PitcherStatForPinchHitter(1, 3), 0 }, { new PitcherStatForPinchHitter(1, 4), 10 }, { new PitcherStatForPinchHitter(1, 5), 10 }, { new PitcherStatForPinchHitter(1, 6), 20 }, { new PitcherStatForPinchHitter(1, 7), 20 }, { new PitcherStatForPinchHitter(1, 8), 30 }, { new PitcherStatForPinchHitter(1, 9), 30 },
            { new PitcherStatForPinchHitter(2, 0), 0 }, { new PitcherStatForPinchHitter(2, 1), 0 }, { new PitcherStatForPinchHitter(2, 2), 0 }, { new PitcherStatForPinchHitter(2, 3), 10 }, { new PitcherStatForPinchHitter(2, 4), 10 }, { new PitcherStatForPinchHitter(2, 5), 20 }, { new PitcherStatForPinchHitter(2, 6), 20 }, { new PitcherStatForPinchHitter(2, 7), 30 }, { new PitcherStatForPinchHitter(2, 8), 30 }, { new PitcherStatForPinchHitter(2, 9), 40 },
            { new PitcherStatForPinchHitter(3, 0), 0 }, { new PitcherStatForPinchHitter(3, 1), 0 }, { new PitcherStatForPinchHitter(3, 2), 0 }, { new PitcherStatForPinchHitter(3, 3), 10 }, { new PitcherStatForPinchHitter(3, 4), 10 }, { new PitcherStatForPinchHitter(3, 5), 20 }, { new PitcherStatForPinchHitter(3, 6), 20 }, { new PitcherStatForPinchHitter(3, 7), 30 }, { new PitcherStatForPinchHitter(3, 8), 30 }, { new PitcherStatForPinchHitter(3, 9), 40 },
            { new PitcherStatForPinchHitter(4, 0), 0 }, { new PitcherStatForPinchHitter(4, 1), 0 }, { new PitcherStatForPinchHitter(4, 2), 0 }, { new PitcherStatForPinchHitter(4, 3), 15 }, { new PitcherStatForPinchHitter(4, 4), 30 }, { new PitcherStatForPinchHitter(4, 5), 40 }, { new PitcherStatForPinchHitter(4, 6), 50 }, { new PitcherStatForPinchHitter(4, 7), 50 }, { new PitcherStatForPinchHitter(4, 8), 60 }, { new PitcherStatForPinchHitter(4, 9), 70 },
            { new PitcherStatForPinchHitter(5, 0), 0 }, { new PitcherStatForPinchHitter(5, 1), 0 }, { new PitcherStatForPinchHitter(5, 2), 25 }, { new PitcherStatForPinchHitter(5, 3), 35 }, { new PitcherStatForPinchHitter(5, 4), 50 }, { new PitcherStatForPinchHitter(5, 5), 60 }, { new PitcherStatForPinchHitter(5, 6), 60 }, { new PitcherStatForPinchHitter(5, 7), 65 }, { new PitcherStatForPinchHitter(5, 8), 75 }, { new PitcherStatForPinchHitter(5, 9), 80 },
            { new PitcherStatForPinchHitter(6, 0), 0 }, { new PitcherStatForPinchHitter(6, 1), 10 }, { new PitcherStatForPinchHitter(6, 2), 30 }, { new PitcherStatForPinchHitter(6, 3), 45 }, { new PitcherStatForPinchHitter(6, 4), 60 }, { new PitcherStatForPinchHitter(6, 5), 70 }, { new PitcherStatForPinchHitter(6, 6), 70 }, { new PitcherStatForPinchHitter(6, 7), 75 }, { new PitcherStatForPinchHitter(6, 8), 80 }, { new PitcherStatForPinchHitter(6, 9), 85 },
            { new PitcherStatForPinchHitter(7, 0), 0 }, { new PitcherStatForPinchHitter(7, 1), 20 }, { new PitcherStatForPinchHitter(7, 2), 45 }, { new PitcherStatForPinchHitter(7, 3), 50 }, { new PitcherStatForPinchHitter(7, 4), 70 }, { new PitcherStatForPinchHitter(7, 5), 70 }, { new PitcherStatForPinchHitter(7, 6), 70 }, { new PitcherStatForPinchHitter(7, 7), 75 }, { new PitcherStatForPinchHitter(7, 8), 80 }, { new PitcherStatForPinchHitter(7, 9), 85 },
            { new PitcherStatForPinchHitter(8, 0), 5 }, { new PitcherStatForPinchHitter(8, 1), 30 }, { new PitcherStatForPinchHitter(8, 2), 60 }, { new PitcherStatForPinchHitter(8, 3), 70 }, { new PitcherStatForPinchHitter(8, 4), 80 }, { new PitcherStatForPinchHitter(8, 5), 90 }, { new PitcherStatForPinchHitter(8, 6), 90 }, { new PitcherStatForPinchHitter(8, 7), 95 }, { new PitcherStatForPinchHitter(8, 8), 95 }, { new PitcherStatForPinchHitter(8, 9), 95 },
            { new PitcherStatForPinchHitter(9, 0), 10 }, { new PitcherStatForPinchHitter(9, 1), 30 }, { new PitcherStatForPinchHitter(9, 2), 70 }, { new PitcherStatForPinchHitter(9, 3), 80 }, { new PitcherStatForPinchHitter(9, 4), 90 }, { new PitcherStatForPinchHitter(9, 5), 100 }, { new PitcherStatForPinchHitter(9, 6), 100 }, { new PitcherStatForPinchHitter(9, 7), 100 }, { new PitcherStatForPinchHitter(9, 8), 100 }, { new PitcherStatForPinchHitter(9, 9), 100 },
        };
        public ChangingPlayer? DecidePinchHitter(GameStatus gameStatus)
        {
            int? pinchHitterPlayerID = null;
            if (gameStatus.inning == 1 && gameStatus.inningStatus == InningStatus.top) return null;
            Player nowBatter = GetBatter();
            Position batterPosition = positions.FirstOrDefault(p => p.Value == (BattingOrder)battingOrder).Key;
            if (gameStatus.inning < 6 && batterPosition != Position.pitcher) return null;
            // 代打を出しても守備交代に問題ないことと
            // 試合場面がクリティカルかどうかとOPSの比較により代打を行うか判定
            // 投手に代打を出す場合は投手の投球成績も考慮
            
            Predicate<int> ableToBecomePinchHitter = (int playerID) => {
                // playerが今代打に出たとしたらというオーダー
                Dictionary<BattingOrder, int> provisionalOrder = order.ToDictionary(entry => entry.Key, entry => entry.Value);
                Dictionary<Position, BattingOrder> provisionalPositions = positions.ToDictionary(entry => entry.Key, entry => entry.Value);
                Dictionary<int, BenchRole> provisionalBenchPlayerIds = benchPlayerIds.ToDictionary(entry => entry.Key, entry => entry.Value);

                provisionalOrder[battingOrder] = playerID;
                provisionalBenchPlayerIds.Remove(playerID);

                // このオーダーが守備について問題を持たないor解決可能かどうかを返す
                ChangingOrderProcessNode parentNode = new ChangingOrderProcessNode(players, player_map, new ChangingOrderProcess(new List<ChangingOrderStep>()), provisionalOrder, provisionalPositions, provisionalBenchPlayerIds);
                return parentNode.IsResoluble();
            };

            // 代打可能（守備に問題が出ない）選手たち
            Dictionary<int, BenchRole> selectablePinchHitters = benchPlayerIds.Where(pair => ableToBecomePinchHitter(pair.Key)).ToDictionary(pair => pair.Key, pair => pair.Value);

            // 重要度に応じて誰を代打に出すかの優先度が決まる
            float OPSDiff; // 代打を出すとしたらという選手とのOPSの差
            float OPSDiffCriteria = 0.0f; // 代打を出すことでこれだけOPSが上がるなら代打を出そうかなという値
            float OPSMinCriteria = 0.0f; // このOPSより高い選手には代打を出す必要は無いかなという値
            List<BenchRole> benchRoles = new List<BenchRole>();
            int importance = gameStatus.CalcImportance();

            // 投手に代打となる場合、先発投手の出来と点差を見て代打を行わないという判断もする
            float stayingPitcherIncentive = 1.0f; // 通常は1.0 続投させたいほど値が小さくなり、OPSMinCriteriaに乗じられる
            if (batterPosition == Position.pitcher) {
                PitcherStatForPinchHitter pitcherStat = nowBatter.pitchingResult.GetPitcherStatForPinchHitter();
                int pinchHitterInsentive = pinchHitterToPitcherIncentives[pitcherStat]; // 0~100
                if (gameStatus.inning > 6 && gameStatus.scoreDiff < 0) {
                    pinchHitterInsentive += 50;
                    if (pinchHitterInsentive > 100) pinchHitterInsentive = 100;
                }
                stayingPitcherIncentive *= 1.0f - (float)pinchHitterInsentive * 0.009f; // *= 0.1~1.0
            }

            if (importance > 80) {
                // 最も重要な局面
                benchRoles = (gameStatus.inning > 6) ? new List<BenchRole> { BenchRole.dependable_pinch_hitter, BenchRole.second_pinch_hitter, BenchRole.third_pinch_hitter, BenchRole.utility, BenchRole.fielder } : new List<BenchRole> { BenchRole.second_pinch_hitter, BenchRole.dependable_pinch_hitter, BenchRole.third_pinch_hitter, BenchRole.utility };
                OPSDiffCriteria = (gameStatus.inning > 6) ? 0.1f : 0.2f;
                OPSMinCriteria = 0.7f * stayingPitcherIncentive;
            } else if (importance > 65) {
                // 重要な局面
                if (gameStatus.inning > 7) {
                    benchRoles = new List<BenchRole> { BenchRole.dependable_pinch_hitter, BenchRole.second_pinch_hitter, BenchRole.third_pinch_hitter, BenchRole.utility };
                    OPSDiffCriteria = 0.2f;
                } else if (gameStatus.inning > 5) {
                    benchRoles = new List<BenchRole> { BenchRole.second_pinch_hitter, BenchRole.dependable_pinch_hitter, BenchRole.third_pinch_hitter };
                    OPSDiffCriteria = 0.3f;
                } else {
                    benchRoles = new List<BenchRole> { BenchRole.second_pinch_hitter, BenchRole.dependable_pinch_hitter, BenchRole.third_pinch_hitter };
                    OPSDiffCriteria = 0.4f;
                }
                OPSMinCriteria = 0.8f * stayingPitcherIncentive;
            } else if (importance > 50) {
                // 少し重要な局面
                benchRoles = (gameStatus.inning > 6) ? new List<BenchRole> { BenchRole.second_pinch_hitter, BenchRole.third_pinch_hitter, BenchRole.dependable_pinch_hitter, BenchRole.utility } : new List<BenchRole> { BenchRole.third_pinch_hitter, BenchRole.utility, BenchRole.second_pinch_hitter, BenchRole.dependable_pinch_hitter };
                OPSMinCriteria = 0.65f * stayingPitcherIncentive;
            }

            pinchHitterPlayerID = SelectPlayerIDByRoles(selectablePinchHitters, benchRoles);
            OPSDiff = (pinchHitterPlayerID.HasValue) ? player_map[pinchHitterPlayerID.Value].battingStatus.Value.ops - nowBatter.battingStatus.Value.ops : 0.0f;;
            bool isMeetOPSDiff = OPSDiff >= OPSDiffCriteria; // OPSの代打による上昇量が基準を超えているか
            bool isMeetOPSMin = (pinchHitterPlayerID.HasValue) ? player_map[pinchHitterPlayerID.Value].battingStatus.Value.ops >= OPSMinCriteria : false; // OPSが基準値を超えているか

            bool execPinchHitting = pinchHitterPlayerID.HasValue && isMeetOPSDiff && isMeetOPSMin;

            ChangingPlayer? changingPlayer = null;
            if (execPinchHitting) {
                // 代打の実行
                int fromPlayerID = order[battingOrder];
                changingPlayer = new ChangingPlayer(fromPlayerID, pinchHitterPlayerID.Value);
                order[battingOrder] = pinchHitterPlayerID.Value;
                players.First(player => player.id == pinchHitterPlayerID.Value).ParticipateGame(gameStatus.inning, battingOrder);
                benchPlayerIds.Remove(pinchHitterPlayerID.Value);
            }

            return changingPlayer;
        }

        public BattingOrder GetBattingOrder()
        {
            return battingOrder;
        }

        public Player GetBatter()
        {
            int playerID = order[battingOrder];
            return player_map[playerID];
        }

        public Player GetPitcher()
        {
            BattingOrder pitcherBattingOrder = positions[Position.pitcher];
            int playerID = order[pitcherBattingOrder];
            return player_map[playerID];
        }

        public void AfterPlay()
        {
            // 打順を進める
            int BOInt = (int)battingOrder;
            BOInt++;
            if (BOInt > (int)BattingOrder.order9) {
                BOInt = (int)BattingOrder.order1;
            }
            battingOrder = (BattingOrder)BOInt;
        }

        // 役割に合うplayerIDを返す。複数プレイヤーがいる場合はランダムに返す
        public int? SelectPlayerIDByRole(Dictionary<int, BenchRole> subjectBenchPlayerIds, BenchRole benchRole)
        {
            List<int> playerIds = subjectBenchPlayerIds.Where(pair => pair.Value == benchRole).ToDictionary(pair => pair.Key, pair => pair.Value).Keys.ToList();
            if (playerIds.Count == 0) {
                return null;
            } else {
                RandomGacha<int> randomPlayerId = new RandomGacha<int>(playerIds.Count);
                foreach (int playerID in playerIds) {
                    randomPlayerId.Add(1, playerID);
                }
                return randomPlayerId.Next();
            }
        }
        public int? SelectPlayerIDByRoles(Dictionary<int, BenchRole> subjectBenchPlayerIds, List<BenchRole> benchRoles)
        {
            int? playerID =  null;
            foreach (BenchRole benchRole in benchRoles) {
                playerID = SelectPlayerIDByRole(subjectBenchPlayerIds, benchRole);
                if (playerID.HasValue) break;
            }
            return playerID;
        }

        public int? FindPlayerIDByRole(BenchRole benchRole)
        {
            return SelectPlayerIDByRole(benchPlayerIds, benchRole);
        }
        public int? FindPlayerIDByRoles(List<BenchRole> benchRoles)
        {
            return SelectPlayerIDByRoles(benchPlayerIds, benchRoles);
        }


	}
}
