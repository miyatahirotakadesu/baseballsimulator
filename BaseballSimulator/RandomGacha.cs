﻿using System;
using System.Collections.Generic;
using System.Linq;

// 使い方
// 1.インスタンス化する
// 2.Addで確率と要素をいくつも入れて行く 確率合計は10000になるように入れる
// 3.Next()したら結果が帰ってくる

namespace BaseballSimulator
{
	public class RandomGacha<T>
	{

        int sumProb; // コンストラクタで設定。合計値がこの値になるように利用しなければならない
        int probSeq = 0;

        Dictionary<T, ResultProb> gifts = new Dictionary<T, ResultProb>();

        static Random cRandom = new System.Random();

        struct ResultProb {
            public int minValue;
            public int maxValue;

            public ResultProb (int minValue, int maxValue) 
            {
                this.minValue = minValue;
                this.maxValue = maxValue;
            }

            public bool InRange(int rndValue)
            {
                return minValue <= rndValue && rndValue < maxValue;
            }
        }

        public RandomGacha(int sumProb)
        {
            this.sumProb = sumProb;
        }

        public void Add(int prob, T gift)
        {
            ResultProb resultProb = new ResultProb(probSeq, probSeq + prob);
            gifts.Add(gift, resultProb);
            probSeq += prob;
        }

        public T Next()
        {
            if (probSeq != sumProb) {
                throw new Exception("確率合計値が " + probSeq + " であり、10000になっていません");
            }
            int rnd = cRandom.Next(0, sumProb); // 0~9999
			T gift = gifts.First (pair => pair.Value.InRange (rnd)).Key; 
            return gift;
        }

	}
}

