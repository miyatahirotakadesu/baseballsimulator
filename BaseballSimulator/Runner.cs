﻿using System;
using System.Diagnostics;
using System.Collections.Generic;

namespace BaseballSimulator
{
	public class Runner
	{
		public Dictionary<Base, int?> playerIDs;
		static RunnerStatus[] IsProbableForceOuts = new RunnerStatus[] { RunnerStatus.first, RunnerStatus.first_second, RunnerStatus.third_first, RunnerStatus.fullbase };
        static Random cRandom = new System.Random();

		public Runner()
		{
			playerIDs = new Dictionary<Base, int?>() {
				{ Base.first, null }, { Base.second, null }, { Base.third, null }
			};
		}

		public RunnerStatus GetRunnerStatus()
		{
			int runnerStatusInt = 0;
			if (playerIDs[Base.first].HasValue == true) {
				runnerStatusInt += 1;
			}
			if (playerIDs[Base.second].HasValue == true) {
				runnerStatusInt += 2;
			}
			if (playerIDs[Base.third].HasValue == true) {
				runnerStatusInt += 4;
			}
			return (RunnerStatus)runnerStatusInt;
		}

		public bool IsProbableForceOut()
		{
			return Array.IndexOf(IsProbableForceOuts, GetRunnerStatus()) != -1;
		}

		public bool HasRunner(Base b)
		{
			return playerIDs[b].HasValue;
		}

        public void ResetRunner()
        {
			playerIDs[Base.first] = null;
			playerIDs[Base.second] = null;
			playerIDs[Base.third] = null;
        }

        static Dictionary<ResultType, Dictionary<Base, Dictionary<int, int>>> moreAdvanceProbs = new Dictionary<ResultType, Dictionary<Base, Dictionary<int, int>>>() {
            {
                ResultType.single,
                new Dictionary<Base, Dictionary<int, int>>{
                    { Base.first, new Dictionary<int, int>{ { 0, 20 }, { 1, 20 }, { 2, 20 } } },
                    { Base.second, new Dictionary<int, int>{ { 0, 30 }, { 1, 50 }, { 2, 75 } } }
                }
            },
            {
                ResultType.twobase,
                new Dictionary<Base, Dictionary<int, int>>{
                    { Base.first, new Dictionary<int, int>{ { 0, 15 }, { 1, 25 }, { 2, 50 } } }
                }
            },
            {
                ResultType.groundout,
                new Dictionary<Base, Dictionary<int, int>>{
                    { Base.first, new Dictionary<int, int>{ { 0, 50 }, { 1, 50 } } },
                    { Base.second, new Dictionary<int, int>{ { 0, 50 }, { 1, 50 } } },
                    { Base.third, new Dictionary<int, int>{ { 0, 50 }, { 1, 50 } } }
                }
            },
            {
                ResultType.flyout,
                new Dictionary<Base, Dictionary<int, int>>{
                    { Base.second, new Dictionary<int, int>{ { 0, 20 }, { 1, 15 } } },
                }
            },
        };

        /*
         * 得点を返す
         */
        public AdvanceResult Advance(ResultPlay resultPlay, int outcount, int batterID)
        {
            /*
                                                0アウト	1アウト	2アウト
                一塁走者が単打で三塁へ進む確率	0.20	0.20	0.20
                二塁走者が単打で生還する確率	0.30	0.50	0.75
                一塁走者が二塁打で生還する確率	0.15	0.25	0.50
                一塁走者が凡打で二塁へ進む確率	0.05	0.05	----
                二塁走者が凡打で三塁へ進む確率	0.25	0.25	----
                三塁走者が凡打で生還する確率	0.50	0.50	----
            */
            ResultType resultType = resultPlay.resultType;
            RunnerStatus initRunnerStatus;
            int homeinCount = 0;
            Position? position = null;
            switch (resultType) {
                case ResultType.single:
                    if (HasRunner(Base.third)) {
                        // 必ずホームイン
                        homeinCount++;
                        playerIDs[Base.third] = null;
                    }
                    if (HasRunner(Base.second)) {
                        int rnd = cRandom.Next(0, 100);
                        if (rnd < moreAdvanceProbs[resultType][Base.second][outcount]) {
                            homeinCount++;
                        } else {
                            playerIDs[Base.third] = playerIDs[Base.second];
                        }
                        playerIDs[Base.second] = null;
                    }
                    if (HasRunner(Base.first)) {
                        int rnd = cRandom.Next(0, 100);
                        if (playerIDs[Base.third] == null && rnd < moreAdvanceProbs[resultType][Base.first][outcount]) {
                            playerIDs[Base.third] = playerIDs[Base.first];
                        } else {
                            playerIDs[Base.second] = playerIDs[Base.first];
                        }
                        playerIDs[Base.first] = null;
                    }
                    playerIDs[Base.first] = batterID;

                    break;

                case ResultType.twobase:
                    if (HasRunner(Base.third)) {
                        homeinCount++;
                        playerIDs[Base.third] = null;
                    }
                    if (HasRunner(Base.second)) {
                        homeinCount++;
                        playerIDs[Base.second] = null;
                    }
                    if (HasRunner(Base.first)) {
                        int rnd = cRandom.Next(0, 100);
                        if (rnd < moreAdvanceProbs[resultType][Base.first][outcount]) {
                            homeinCount++;
                        } else {
                            playerIDs[Base.third] = playerIDs[Base.first];
                        }
                        playerIDs[Base.first] = null;
                    }
                    playerIDs[Base.second] = batterID;

                    break;

                case ResultType.triple:
                    if (HasRunner(Base.third)) {
                        homeinCount++;
                        playerIDs[Base.third] = null;
                    }
                    if (HasRunner(Base.second)) {
                        homeinCount++;
                        playerIDs[Base.second] = null;
                    }
                    if (HasRunner(Base.first)) {
                        homeinCount++;
                        playerIDs[Base.first] = null;
                    }
                    playerIDs[Base.third] = batterID;

                    break;

                case ResultType.homerun:
                    if (HasRunner(Base.third)) {
                        homeinCount++;
                        playerIDs[Base.third] = null;
                    }
                    if (HasRunner(Base.second)) {
                        homeinCount++;
                        playerIDs[Base.second] = null;
                    }
                    if (HasRunner(Base.first)) {
                        homeinCount++;
                        playerIDs[Base.first] = null;
                    }
                    homeinCount++;

                    break;

                case ResultType.fourball:
                    if (HasRunner(Base.first)) {
                        if (HasRunner(Base.second)) {
                            if (HasRunner(Base.third)) {
                                homeinCount++;
                            }
                            playerIDs[Base.third] = playerIDs[Base.second];
                        }
                        playerIDs[Base.second] = playerIDs[Base.first];
                    }
                    playerIDs[Base.first] = batterID;

                    break;

                case ResultType.groundout:
                    Dictionary<Base, bool> advancedMap = new Dictionary<Base, bool>() {
                        { Base.first, false }, { Base.second, false }, { Base.third, false }
                    };
                    initRunnerStatus = GetRunnerStatus();
                    if (HasRunner(Base.third)) {
                        int rnd = cRandom.Next(0, 100);
                        if (rnd < moreAdvanceProbs[resultType][Base.third][outcount]) {
                            homeinCount++;
                            playerIDs[Base.third] = null;
                            advancedMap[Base.third] = true;
                        }
                    }
                    if (HasRunner(Base.second)) {
                        int rnd = cRandom.Next(0, 100);
                        if (playerIDs[Base.third] == null && rnd < moreAdvanceProbs[resultType][Base.second][outcount]) {
                            playerIDs[Base.third] = playerIDs[Base.second];
                            playerIDs[Base.second] = null;
                            advancedMap[Base.second] = true;
                        }
                    }
                    if (HasRunner(Base.first)) {
                        int rnd = cRandom.Next(0, 100);
                        if (playerIDs[Base.second] == null && rnd < moreAdvanceProbs[resultType][Base.second][outcount]) {
                            playerIDs[Base.second] = playerIDs[Base.first];
                            playerIDs[Base.first] = null;
                            advancedMap[Base.first] = true;
                        }
                    }

                    // 進塁失敗した走者か打者のどちらかをアウトにする
                    switch (initRunnerStatus) {
                        case RunnerStatus.first:
                            if (advancedMap[Base.first] == false) {
                                playerIDs[Base.first] = batterID;
                            }
                            break;
                        case RunnerStatus.second:
                            if (advancedMap[Base.second] == false) {
                                int rnd = cRandom.Next(0, 100);
                                if (rnd < 40) {
                                    // ２塁走者アウト
                                    playerIDs[Base.first] = null;
                                    playerIDs[Base.first] = batterID;
                                }
                            }
                            break;
                        case RunnerStatus.first_second:
                            if (advancedMap[Base.second] == false && advancedMap[Base.first] == false) {
                                playerIDs[Base.second] = playerIDs[Base.first];
                                playerIDs[Base.first] = batterID;
                            } else if (advancedMap[Base.second] == true && advancedMap[Base.first] == false) {
                                playerIDs[Base.first] = batterID;
                            }
                            break;
                        case RunnerStatus.third_first:
                            if (advancedMap[Base.third] == false && advancedMap[Base.first] == false) {
                                playerIDs[Base.first] = batterID;
                            } else if (advancedMap[Base.third] == true && advancedMap[Base.first] == false) {
                                playerIDs[Base.first] = batterID;
                            } else if (advancedMap[Base.third] == false && advancedMap[Base.first] == true) {
                                playerIDs[Base.third] = null;
                                playerIDs[Base.first] = batterID;
                            }
                            break;
                        case RunnerStatus.third_second:
                            if (advancedMap[Base.third] == false && advancedMap[Base.second] == false) {
                                // ２・３塁ランナーそのまま自重
                            } else if (advancedMap[Base.third] == true && advancedMap[Base.second] == false) {
                                // ２塁ランナーだけそのまま自重
                            }
                            break;
                        case RunnerStatus.fullbase:
                            if (advancedMap[Base.third] == false && advancedMap[Base.second] == false && advancedMap[Base.first] == false) {
                                playerIDs[Base.third] = playerIDs[Base.third];
                                playerIDs[Base.second] = playerIDs[Base.first];
                                playerIDs[Base.first] = batterID;
                            } else if (advancedMap[Base.third] == true && advancedMap[Base.second] == false && advancedMap[Base.first] == false) {
                                playerIDs[Base.third] = null;
                                playerIDs[Base.second] = playerIDs[Base.first];
                                playerIDs[Base.first] = batterID;
                            } else if (advancedMap[Base.third] == true && advancedMap[Base.second] == true && advancedMap[Base.first] == false) {
                                playerIDs[Base.second] = null;
                                playerIDs[Base.first] = batterID;
                            }
                            break;
                    }
                    break;

                case ResultType.flyout:
                    if (HasRunner(Base.second)) {
                        int rnd = cRandom.Next(0, 100);
                        if (playerIDs[Base.third] == null && rnd < moreAdvanceProbs[resultType][Base.second][outcount]) {
                            playerIDs[Base.third] = playerIDs[Base.second];
                            playerIDs[Base.second] = null;
                        }
                    }
                    break;

                case ResultType.sacrifice_bunt:
                    if (HasRunner(Base.third)) {
                        homeinCount++;
                        playerIDs[Base.third] = null;
                    }
                    if (HasRunner(Base.second)) {
                        playerIDs[Base.third] = playerIDs[Base.second];
                        playerIDs[Base.second] = null;
                    }
                    if (HasRunner(Base.first)) {
                        playerIDs[Base.second] = playerIDs[Base.first];
                        playerIDs[Base.first] = null;
                    }
                    break;

                case ResultType.sacrifice_fly:
                    homeinCount++;
                    playerIDs[Base.third] = null;
                    break;

                case ResultType.doubleplay:
                    initRunnerStatus = GetRunnerStatus();
                    switch (initRunnerStatus) {
                        case RunnerStatus.first:
                            playerIDs[Base.first] = null;
                            break;
                        case RunnerStatus.first_second:
                            playerIDs[Base.third] = playerIDs[Base.second];
                            playerIDs[Base.second] = null;
                            playerIDs[Base.first] = null;
                            break;
                        case RunnerStatus.third_first:
                            homeinCount++;
                            playerIDs[Base.third] = null;
                            break;
                        case RunnerStatus.fullbase:
                            int rnd = cRandom.Next(0, 100);
                            if (rnd < moreAdvanceProbs[ResultType.groundout][Base.third][outcount]) {
                                // 満塁で6-4-3ダブルプレー
                                homeinCount++;
                                playerIDs[Base.third] = playerIDs[Base.second];
                                playerIDs[Base.second] = null;
                                playerIDs[Base.first] = null;
                            } else {
                                // 満塁でホームゲッツー
                                playerIDs[Base.third] = playerIDs[Base.second];
                                playerIDs[Base.second] = playerIDs[Base.first];
                                playerIDs[Base.first] = null;
                            }
                            break;
                    }

                    break;

                case ResultType.foulfly:
                    // do nathong
                    break;

            }

            return new AdvanceResult(homeinCount, position);
        }


	}

	public struct AdvanceResult
    {
        public int homeinCount; // 何人ホームインしたか
        public Position? position; // どの守備位置への打球か
        public AdvanceResult(int homeinCount, Position? position) {
            this.homeinCount = homeinCount;
            this.position = position;
        }
    }
}

