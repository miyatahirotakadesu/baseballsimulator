﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace BaseballSimulator
{
	// 打順
	public enum BattingOrder { order0, order1, order2, order3, order4, order5, order6, order7, order8, order9 };

	// 守備位置
    [JsonConverter(typeof(StringEnumConverter))]
	public enum Position {
        [EnumMember(Value = "pitcher")]
        pitcher = 1,
        [EnumMember(Value = "catcher")]
        catcher,
        [EnumMember(Value = "first")]
        first,
        [EnumMember(Value = "second")]
        second,
        [EnumMember(Value = "third")]
        third,
        [EnumMember(Value = "shortstop")]
        shortstop,
        [EnumMember(Value = "left")]
        left,
        [EnumMember(Value = "center")]
        center,
        [EnumMember(Value = "right")]
        right,
        [EnumMember(Value = "designated_hitter")]
        designated_hitter
    };

	// 役割
    [JsonConverter(typeof(StringEnumConverter))]
	public enum BenchRole {
        [EnumMember(Value = "dependable_pinch_hitter")]
        dependable_pinch_hitter,
        [EnumMember(Value = "second_pinch_hitter")]
        second_pinch_hitter,
        [EnumMember(Value = "third_pinch_hitter")]
        third_pinch_hitter,
        [EnumMember(Value = "utility")]
        utility,
        [EnumMember(Value = "fielder")]
        fielder,
        [EnumMember(Value = "long_relief")]
        long_relief,
        [EnumMember(Value = "normal_relief")]
        normal_relief,
        [EnumMember(Value = "short_relief")]
        short_relief,
        [EnumMember(Value = "pre_setup_pitcher")]
        pre_setup_pitcher,
        [EnumMember(Value = "setup_pitcher")]
        setup_pitcher,
        [EnumMember(Value = "closer")]
        closer
    };

	// 表裏
    [JsonConverter(typeof(StringEnumConverter))]
	public enum InningStatus {
        [EnumMember(Value = "top")]
        top,
        [EnumMember(Value = "bottom")]
        bottom
    };

	// 攻撃か守備か
	public enum ODType { offence, defence };

	// ホームかビジターか
	public enum TeamType { visitor, home };

	// ベース
	public enum Base { first, second, third, home };

	// ランナーの状態
	public enum RunnerStatus { none, first, second, first_second, third, third_first, third_second, fullbase };

	// 作戦
    [JsonConverter(typeof(StringEnumConverter))]
	public enum TeamStrategy {
        [EnumMember(Value = "hitting")]
        hitting,
        [EnumMember(Value = "bunt")]
        bunt,
        [EnumMember(Value = "steal")]
        steal
    };

	// 結果
    [JsonConverter(typeof(StringEnumConverter))]
	public enum ResultType {
        [EnumMember(Value = "single")]
        single,
        [EnumMember(Value = "twobase")]
        twobase,
        [EnumMember(Value = "triple")]
        triple,
        [EnumMember(Value = "homerun")]
        homerun,
        [EnumMember(Value = "fourball")]
        fourball,
        [EnumMember(Value = "strikeout")]
        strikeout,
        [EnumMember(Value = "groundout")]
        groundout,
        [EnumMember(Value = "flyout")]
        flyout,
        [EnumMember(Value = "sacrifice_bunt")]
        sacrifice_bunt,
        [EnumMember(Value = "sacrifice_fly")]
        sacrifice_fly,
        [EnumMember(Value = "doubleplay")]
        doubleplay,
        [EnumMember(Value = "foulfly")]
        foulfly
    };

	public class Util {

		public static TEnum ConvertToEnum<TEnum> (string value)
		{
			return (TEnum) Enum.Parse (typeof (TEnum), value);
		}

		public static TEnum ConvertToEnum<TEnum> (int number)
		{
			return (TEnum)Enum.ToObject (typeof(TEnum), number);
		}

	}

}
