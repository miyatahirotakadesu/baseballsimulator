﻿using System;
using System.Collections.Generic;

namespace BaseballSimulator
{
    // 試合状況と乱数から采配を決定
	public class GameStrategy
	{

        enum InsentiveType { NEXT_ONE_SCORE, NEXT_TWO_SCORE };

        // バントや盗塁を行いたい場面かどうかの表
        // 100点満点で、「次の１点の重み」と「次の２点の重み」を表す
        static Dictionary<int, Dictionary<InsentiveType, int>> toThirdInningStrategyInsentives = new Dictionary<int, Dictionary<InsentiveType, int>>(){
            { -4, new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 0 },  { InsentiveType.NEXT_TWO_SCORE, 5 } } },
            { -3, new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 0 },  { InsentiveType.NEXT_TWO_SCORE, 5 } } },
            { -2, new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 5 },  { InsentiveType.NEXT_TWO_SCORE, 10 } } },
            { -1, new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 10 }, { InsentiveType.NEXT_TWO_SCORE, 20 } } },
            { 0,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 10 }, { InsentiveType.NEXT_TWO_SCORE, 20 } } },
            { 1,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 10 }, { InsentiveType.NEXT_TWO_SCORE, 20 } } },
            { 2,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 10 }, { InsentiveType.NEXT_TWO_SCORE, 20 } } },
            { 3,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 10 }, { InsentiveType.NEXT_TWO_SCORE, 20 } } },
            { 4,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 10 }, { InsentiveType.NEXT_TWO_SCORE, 20 } } },
        };
        static Dictionary<int, Dictionary<InsentiveType, int>> toFifthInningStrategyInsentives = new Dictionary<int, Dictionary<InsentiveType, int>>(){
            { -4, new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 0 },  { InsentiveType.NEXT_TWO_SCORE, 5 } } },
            { -3, new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 0 },  { InsentiveType.NEXT_TWO_SCORE, 5 } } },
            { -2, new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 5 },  { InsentiveType.NEXT_TWO_SCORE, 10 } } },
            { -1, new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 15 }, { InsentiveType.NEXT_TWO_SCORE, 25 } } },
            { 0,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 15 }, { InsentiveType.NEXT_TWO_SCORE, 25 } } },
            { 1,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 15 }, { InsentiveType.NEXT_TWO_SCORE, 25 } } },
            { 2,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 10 }, { InsentiveType.NEXT_TWO_SCORE, 20 } } },
            { 3,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 10 }, { InsentiveType.NEXT_TWO_SCORE, 20 } } },
            { 4,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 10 }, { InsentiveType.NEXT_TWO_SCORE, 20 } } },
        };
        static Dictionary<int, Dictionary<InningStatus, Dictionary<int, Dictionary<InsentiveType, int>>>> StrategyInsentives
            = new Dictionary<int, Dictionary<InningStatus, Dictionary<int, Dictionary<InsentiveType, int>>>>() {
            // [inning][InningStatus][scoreDiff][InsentiveType] = prob
                { 1, new Dictionary<InningStatus, Dictionary<int, Dictionary<InsentiveType, int>>>
                    { { InningStatus.top, toThirdInningStrategyInsentives }, { InningStatus.bottom, toThirdInningStrategyInsentives }, }
                },
                { 2, new Dictionary<InningStatus, Dictionary<int, Dictionary<InsentiveType, int>>>
                    { { InningStatus.top, toThirdInningStrategyInsentives }, { InningStatus.bottom, toThirdInningStrategyInsentives }, }
                },
                { 3, new Dictionary<InningStatus, Dictionary<int, Dictionary<InsentiveType, int>>>
                    { { InningStatus.top, toThirdInningStrategyInsentives }, { InningStatus.bottom, toThirdInningStrategyInsentives }, }
                },
                { 4, new Dictionary<InningStatus, Dictionary<int, Dictionary<InsentiveType, int>>>
                    { { InningStatus.top, toFifthInningStrategyInsentives }, { InningStatus.bottom, toFifthInningStrategyInsentives }, }
                },
                { 5, new Dictionary<InningStatus, Dictionary<int, Dictionary<InsentiveType, int>>>
                    { { InningStatus.top, toFifthInningStrategyInsentives }, { InningStatus.bottom, toFifthInningStrategyInsentives }, }
                },
                { 6, new Dictionary<InningStatus, Dictionary<int, Dictionary<InsentiveType, int>>>
                    {
                        { InningStatus.top, new Dictionary<int, Dictionary<InsentiveType, int>>{
                            { -4, new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 0 },  { InsentiveType.NEXT_TWO_SCORE, 0 } } },
                            { -3, new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 0 },  { InsentiveType.NEXT_TWO_SCORE, 5 } } },
                            { -2, new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 20 }, { InsentiveType.NEXT_TWO_SCORE, 40 } } },
                            { -1, new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 30 }, { InsentiveType.NEXT_TWO_SCORE, 40 } } },
                            { 0,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 40 }, { InsentiveType.NEXT_TWO_SCORE, 50 } } },
                            { 1,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 40 }, { InsentiveType.NEXT_TWO_SCORE, 50 } } },
                            { 2,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 40 }, { InsentiveType.NEXT_TWO_SCORE, 50 } } },
                            { 3,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 30 }, { InsentiveType.NEXT_TWO_SCORE, 40 } } },
                            { 4,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 30 }, { InsentiveType.NEXT_TWO_SCORE, 40 } } },
                        } },
                        { InningStatus.bottom, new Dictionary<int, Dictionary<InsentiveType, int>>{
                            { -4, new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 0 },  { InsentiveType.NEXT_TWO_SCORE, 5 } } },
                            { -3, new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 0 },  { InsentiveType.NEXT_TWO_SCORE, 5 } } },
                            { -2, new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 20 }, { InsentiveType.NEXT_TWO_SCORE, 40 } } },
                            { -1, new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 30 }, { InsentiveType.NEXT_TWO_SCORE, 40 } } },
                            { 0,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 40 }, { InsentiveType.NEXT_TWO_SCORE, 40 } } },
                            { 1,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 40 }, { InsentiveType.NEXT_TWO_SCORE, 40 } } },
                            { 2,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 40 }, { InsentiveType.NEXT_TWO_SCORE, 40 } } },
                            { 3,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 40 }, { InsentiveType.NEXT_TWO_SCORE, 40 } } },
                            { 4,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 30 }, { InsentiveType.NEXT_TWO_SCORE, 30 } } },
                        } }
                    }
                },
                { 7, new Dictionary<InningStatus, Dictionary<int, Dictionary<InsentiveType, int>>>
                    {
                        { InningStatus.top, new Dictionary<int, Dictionary<InsentiveType, int>>{
                            { -4, new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 0 },  { InsentiveType.NEXT_TWO_SCORE, 0 } } },
                            { -3, new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 0 },  { InsentiveType.NEXT_TWO_SCORE, 0 } } },
                            { -2, new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 10 }, { InsentiveType.NEXT_TWO_SCORE, 50 } } },
                            { -1, new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 30 }, { InsentiveType.NEXT_TWO_SCORE, 50 } } },
                            { 0,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 60 }, { InsentiveType.NEXT_TWO_SCORE, 40 } } },
                            { 1,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 60 }, { InsentiveType.NEXT_TWO_SCORE, 40 } } },
                            { 2,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 60 }, { InsentiveType.NEXT_TWO_SCORE, 40 } } },
                            { 3,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 60 }, { InsentiveType.NEXT_TWO_SCORE, 40 } } },
                            { 4,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 40 }, { InsentiveType.NEXT_TWO_SCORE, 30 } } },
                        } },
                        { InningStatus.bottom, new Dictionary<int, Dictionary<InsentiveType, int>>{
                            { -4, new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 0 },  { InsentiveType.NEXT_TWO_SCORE, 0 } } },
                            { -3, new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 0 },  { InsentiveType.NEXT_TWO_SCORE, 0 } } },
                            { -2, new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 10 }, { InsentiveType.NEXT_TWO_SCORE, 60 } } },
                            { -1, new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 40 }, { InsentiveType.NEXT_TWO_SCORE, 60 } } },
                            { 0,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 70 }, { InsentiveType.NEXT_TWO_SCORE, 30 } } },
                            { 1,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 70 }, { InsentiveType.NEXT_TWO_SCORE, 30 } } },
                            { 2,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 70 }, { InsentiveType.NEXT_TWO_SCORE, 30 } } },
                            { 3,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 70 }, { InsentiveType.NEXT_TWO_SCORE, 30 } } },
                            { 4,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 50 }, { InsentiveType.NEXT_TWO_SCORE, 30 } } },
                        } }
                    }
                },
                { 8, new Dictionary<InningStatus, Dictionary<int, Dictionary<InsentiveType, int>>>
                    {
                        { InningStatus.top, new Dictionary<int, Dictionary<InsentiveType, int>>{
                            { -4, new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 0 },  { InsentiveType.NEXT_TWO_SCORE, 0 } } },
                            { -3, new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 0 },  { InsentiveType.NEXT_TWO_SCORE, 0 } } },
                            { -2, new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 0 },  { InsentiveType.NEXT_TWO_SCORE, 60 } } },
                            { -1, new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 60 }, { InsentiveType.NEXT_TWO_SCORE, 80 } } },
                            { 0,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 70 }, { InsentiveType.NEXT_TWO_SCORE, 30 } } },
                            { 1,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 70 }, { InsentiveType.NEXT_TWO_SCORE, 30 } } },
                            { 2,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 70 }, { InsentiveType.NEXT_TWO_SCORE, 30 } } },
                            { 3,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 70 }, { InsentiveType.NEXT_TWO_SCORE, 30 } } },
                            { 4,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 50 }, { InsentiveType.NEXT_TWO_SCORE, 20 } } },
                        } },
                        { InningStatus.bottom, new Dictionary<int, Dictionary<InsentiveType, int>>{
                            { -4, new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 0 },  { InsentiveType.NEXT_TWO_SCORE, 0 } } },
                            { -3, new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 0 },  { InsentiveType.NEXT_TWO_SCORE, 0 } } },
                            { -2, new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 0 },  { InsentiveType.NEXT_TWO_SCORE, 70 } } },
                            { -1, new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 70 }, { InsentiveType.NEXT_TWO_SCORE, 90 } } },
                            { 0,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 90 }, { InsentiveType.NEXT_TWO_SCORE, 10 } } },
                            { 1,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 90 }, { InsentiveType.NEXT_TWO_SCORE, 10 } } },
                            { 2,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 90 }, { InsentiveType.NEXT_TWO_SCORE, 10 } } },
                            { 3,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 80 }, { InsentiveType.NEXT_TWO_SCORE, 10 } } },
                            { 4,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 60 }, { InsentiveType.NEXT_TWO_SCORE, 10 } } },
                        } }
                    }
                },
                { 9, new Dictionary<InningStatus, Dictionary<int, Dictionary<InsentiveType, int>>>
                    {
                        { InningStatus.top, new Dictionary<int, Dictionary<InsentiveType, int>>{
                            { -4, new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 0 },  { InsentiveType.NEXT_TWO_SCORE, 0 } } },
                            { -3, new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 0 },  { InsentiveType.NEXT_TWO_SCORE, 0 } } },
                            { -2, new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 0 },  { InsentiveType.NEXT_TWO_SCORE, 70 } } },
                            { -1, new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 70 }, { InsentiveType.NEXT_TWO_SCORE, 100 } } },
                            { 0,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 90 }, { InsentiveType.NEXT_TWO_SCORE, 10 } } },
                            { 1,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 90 }, { InsentiveType.NEXT_TWO_SCORE, 10 } } },
                            { 2,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 90 }, { InsentiveType.NEXT_TWO_SCORE, 10 } } },
                            { 3,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 80 }, { InsentiveType.NEXT_TWO_SCORE, 10 } } },
                            { 4,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 60 }, { InsentiveType.NEXT_TWO_SCORE, 10 } } },
                        } },
                        { InningStatus.bottom, new Dictionary<int, Dictionary<InsentiveType, int>>{
                            { -4, new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 0 },   { InsentiveType.NEXT_TWO_SCORE, 0 } } },
                            { -3, new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 0 },   { InsentiveType.NEXT_TWO_SCORE, 0 } } },
                            { -2, new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 0 },   { InsentiveType.NEXT_TWO_SCORE, 80 } } },
                            { -1, new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 70 },  { InsentiveType.NEXT_TWO_SCORE, 100 } } },
                            { 0,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 100 }, { InsentiveType.NEXT_TWO_SCORE, 0 } } },
                            { 1,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 0 },   { InsentiveType.NEXT_TWO_SCORE, 0 } } },
                            { 2,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 0 },   { InsentiveType.NEXT_TWO_SCORE, 0 } } },
                            { 3,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 0 },   { InsentiveType.NEXT_TWO_SCORE, 0 } } },
                            { 4,  new Dictionary<InsentiveType, int>{ { InsentiveType.NEXT_ONE_SCORE, 0 },   { InsentiveType.NEXT_TWO_SCORE, 0 } } },
                        } }
                    }
                }
            };

        // 塁上の状況により決まる盗塁を行いたい場面かどうかの表
        static Dictionary<RunnerStatus,Dictionary<InsentiveType, float>> stealInsentivesByRunnerSituations = new Dictionary<RunnerStatus,Dictionary<InsentiveType, float>>() {
            { RunnerStatus.first, new Dictionary<InsentiveType, float>{ { InsentiveType.NEXT_ONE_SCORE, 1.0f }, { InsentiveType.NEXT_TWO_SCORE, -1.0f } } }, // 「次の１点」の重みが大きく、「次の２点」の重みが小さいときに行いがち
            { RunnerStatus.third_first, new Dictionary<InsentiveType, float>{ { InsentiveType.NEXT_ONE_SCORE, -1.0f }, { InsentiveType.NEXT_TWO_SCORE, 1.0f } }  }, // 「次の１点」の重みが小さく、「次の２点」の重みが大きいときに行いがち
        };

        // 塁上の状況により決まるバントを行いたい場面かどうかの表
        static Dictionary<RunnerStatus,Dictionary<InsentiveType, float>> buntInsentivesByRunnerSituations = new Dictionary<RunnerStatus,Dictionary<InsentiveType, float>>() {
            { RunnerStatus.first, new Dictionary<InsentiveType, float>{ { InsentiveType.NEXT_ONE_SCORE, 1.0f }, { InsentiveType.NEXT_TWO_SCORE, -1.0f } } }, // 「次の１点」の重みが大きく、「次の２点」の重みが小さいときに行いがち
            { RunnerStatus.second, new Dictionary<InsentiveType, float>{ { InsentiveType.NEXT_ONE_SCORE, 0.6f }, { InsentiveType.NEXT_TWO_SCORE, -1.0f } } }, // 「次の１点」の重みが大きく、「次の２点」の重みが小さいときに行いがち
            { RunnerStatus.first_second, new Dictionary<InsentiveType, float>{ { InsentiveType.NEXT_ONE_SCORE, 0.6f }, { InsentiveType.NEXT_TWO_SCORE, 1.0f } } }, // 「次の２点」と「次の１点」の両方の重みが大きいほど行いがち
            { RunnerStatus.third, new Dictionary<InsentiveType, float>{ { InsentiveType.NEXT_ONE_SCORE, 0.1f }, { InsentiveType.NEXT_TWO_SCORE, -1.0f } }  }, // 「次の１点」の重みが大きく、「次の２点」の重みが小さいときに行いがち。スクイズになるので控えめに
            { RunnerStatus.third_first, new Dictionary<InsentiveType, float>{ { InsentiveType.NEXT_ONE_SCORE, 0.1f }, { InsentiveType.NEXT_TWO_SCORE, 0.2f } }  }, // 「次の２点」と「次の１点」の両方の重みが大きいほど行いがち。スクイズになるので控えめに
            { RunnerStatus.third_second, new Dictionary<InsentiveType, float>{ { InsentiveType.NEXT_ONE_SCORE, 0.1f }, { InsentiveType.NEXT_TWO_SCORE, 0.2f } } }, // 「次の２点」と「次の１点」の両方の重みが大きいほど行いがち。スクイズになるので控えめに
        };

        static Random cRandom = new System.Random();
            
        public int inning;
        public InningStatus inningStatus;
        public int outcount;
        public RunnerStatus runnerStatus;
        public int scoreDiff;
        public int offenceTeamScore;

        public GameStrategy(GameStatus gameStatus)
        {
            int inning = gameStatus.inning;
            if (inning > 9) inning = 9;
            this.inning = inning;
            this.inningStatus = gameStatus.inningStatus;
            this.outcount = gameStatus.outcount;
            this.runnerStatus = gameStatus.runnerStatus;
            int diff = gameStatus.scoreDiff;
            if (diff > 4) {
                diff = 4;
            } else if (diff < -4) {
                diff = -4;
            }
            this.scoreDiff = diff;
            this.offenceTeamScore = gameStatus.offenceTeamScore;
        }

        public TeamStrategy DecideTeamStrategy()
        {
            if (IsExecutingSteal() == true) {
                return TeamStrategy.steal;
            } else if (IsExecutingBunt() == true) {
                return TeamStrategy.bunt;
            } else {
                return TeamStrategy.hitting;
            }
        }

        /*
         * 盗塁を実行するかどうかを、状況と乱数から決定し結果を返す
         * return $is_execting_steal Boolean
         */
        bool IsExecutingSteal() {
            // 塁上の状況からが盗塁を行うことがありえない場面の場合
            if (stealInsentivesByRunnerSituations.ContainsKey(runnerStatus) == false) {
                return false;
            }

            Dictionary<InsentiveType, float> stealInsentive = stealInsentivesByRunnerSituations[runnerStatus];
            Dictionary<InsentiveType, int> strategyInsentive = StrategyInsentives[inning][inningStatus][scoreDiff];
            /*
             * 例
             * --前提ここから--
             * self::RSTYPE_FIRST => array('next_one_score' => 1.0, 'next_two_score' => -1.0), // 「次の１点」の重みが大きく、「次の２点」の重みが小さいときに行いがち
             * self::RSTYPE_FIRST_THIRD => array('next_one_score' => -1.0, 'next_two_score' => 1.0), // 「次の１点」の重みが小さく、「次の２点」の重みが大きいときに行いがち
             * '8' => array(
             *     'top' => array(
             *         '-4' => array('next_one_score' => 0, 'next_two_score' => 0),
             *         '-3' => array('next_one_score' => 0, 'next_two_score' => 0),
             *         '-2' => array('next_one_score' => 0, 'next_two_score' => 60),
             *         '-1' => array('next_one_score' => 60, 'next_two_score' => 80),
             *         '0'  => array('next_one_score' => 70, 'next_two_score' => 30),
             *         '1'  => array('next_one_score' => 70, 'next_two_score' => 30),
             *         '2'  => array('next_one_score' => 70, 'next_two_score' => 30),
             *         '3'  => array('next_one_score' => 70, 'next_two_score' => 30),
             *         '4'  => array('next_one_score' => 50, 'next_two_score' => 20),
             *     )
             * )
             * --前提ここまで--
             *
             * 例A.8表1点リード 1塁の場合
             * $steal_prob_a = 70 * 1.0 = 70
             * $steal_prob_b = 70 * (-1.0 * 30 / 100) = -21
             * $steal_prob = 70 - 21 = 59
             *
             * 例B.8表1点ビハインド 1,3塁の場合
             * $steal_prob_a = 80 * 1.0 = 80
             * $steal_prob_b = 80 * (-1.0 * 60 / 100) = -48
             * $steal_prob = 80 - 48 = 32
             */

            float stealProbA = (stealInsentive[InsentiveType.NEXT_ONE_SCORE] > 0) ? (float)strategyInsentive[InsentiveType.NEXT_ONE_SCORE] * stealInsentive[InsentiveType.NEXT_ONE_SCORE] : (float)strategyInsentive[InsentiveType.NEXT_TWO_SCORE] * stealInsentive[InsentiveType.NEXT_TWO_SCORE]; // steal_prob_aは必ず正の値になる
            if (stealProbA < 0) throw new Exception("盗塁実行確率計算に不具合があります");

            float stealProbBRate = (stealInsentive[InsentiveType.NEXT_ONE_SCORE] > 0) ? (float)strategyInsentive[InsentiveType.NEXT_TWO_SCORE] * stealInsentive[InsentiveType.NEXT_TWO_SCORE] / 100 : (float)strategyInsentive[InsentiveType.NEXT_ONE_SCORE] * stealInsentive[InsentiveType.NEXT_ONE_SCORE] / 100;
            float stealProbB = stealProbA * stealProbBRate;

            int stealProb = Convert.ToInt32(stealProbA + stealProbB);
            int rnd = cRandom.Next(0, 100); // 0~99

            return rnd < stealProb;
        }

        /*
         * バントを実行するかどうかを、状況と乱数から決定し結果を返す
         * return $is_execting_bunt Boolean
         */
        bool IsExecutingBunt() {
            // 塁上の状況・アウトカウントからがバントを行うことがありえない場面の場合
            if (buntInsentivesByRunnerSituations.ContainsKey(runnerStatus) == false || outcount == 2) {
                return false;
            }

            Dictionary<InsentiveType, float> buntInsentive = buntInsentivesByRunnerSituations[runnerStatus];
            Dictionary<InsentiveType, int> strategyInsentive = StrategyInsentives[inning][inningStatus][scoreDiff];

            /*
             * 例
             * --前提ここから--
             * self::RSTYPE_SECOND => array('next_one_score' => 0.6, 'next_two_score' => -1.0),
             * self::RSTYPE_FIRST_SECOND => array('next_one_score' => 0.6, 'next_two_score' => 1.0),
             * self::RSTYPE_THIRD => array('next_one_score' => 0.1, 'next_two_score' => -1.0),
             * '8' => array(
             *     'top' => array(
             *         '-4' => array('next_one_score' => 0, 'next_two_score' => 0),
             *         '-3' => array('next_one_score' => 0, 'next_two_score' => 0),
             *         '-2' => array('next_one_score' => 0, 'next_two_score' => 60),
             *         '-1' => array('next_one_score' => 60, 'next_two_score' => 80),
             *         '0'  => array('next_one_score' => 70, 'next_two_score' => 30),
             *         '1'  => array('next_one_score' => 70, 'next_two_score' => 30),
             *         '2'  => array('next_one_score' => 70, 'next_two_score' => 30),
             *         '3'  => array('next_one_score' => 70, 'next_two_score' => 30),
             *         '4'  => array('next_one_score' => 50, 'next_two_score' => 20),
             *     )
             * )
             * --前提ここまで--
             *
             * 例A.8表1点リード 1,2塁の場合
             * $bunt_prob_a = 70 * 0.6 = 42
             * $bunt_prob_b = (100 - 42) * (1.0 * 30 / 100) = 17.4
             * $bunt_prob = 42 + 17.4 = 59.6
             *
             * 例B.8表1点ビハインド 2塁の場合
             * $bunt_prob_a = 60 * 0.6 = 36
             * $bunt_prob_b = 36 * (-1.0 * 80 / 100) = -28.8
             * $bunt_prob = 36 - 28.8 = 7.8
             */

            float buntProbA = (float)strategyInsentive[InsentiveType.NEXT_ONE_SCORE] * buntInsentive[InsentiveType.NEXT_ONE_SCORE];
            float buntProbBBase = (buntInsentive[InsentiveType.NEXT_TWO_SCORE] > 0) ? 100.0f - buntProbA : buntProbA;
            float buntProbB = buntProbBBase * ((float)strategyInsentive[InsentiveType.NEXT_TWO_SCORE] * buntInsentive[InsentiveType.NEXT_TWO_SCORE] / 100);
            float buntProb = buntProbA + buntProbB;

            if (outcount == 1) {
                buntProb *= 0.4f; // １アウトからもバントは渋ろう
            }

            // 打撃戦ならバントを渋る、投手戦なら積極的にバント
            if (inning > 6 && offenceTeamScore <= 1) {
                buntProb *= 1.3f;
            } else if ((float)offenceTeamScore / (float)inning > 0.8f) {
                buntProb *= 0.1f;
            } else if ((float)offenceTeamScore / (float)inning > 0.5f) {
                buntProb *= 0.5f;
            }
            int rnd = cRandom.Next(0, 100); // 0~99

            return rnd < Convert.ToInt32(buntProb);
        }

	}
}

