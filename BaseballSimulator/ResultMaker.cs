﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace BaseballSimulator
{
	public class ResultMaker
	{
        public int single;
        public int twobase;
        public int triple;
        public int homerun;
        public int strikeout;
        public int fourball;
        public int groundout;
        public int flyout;

        static Random cRandom = new System.Random();

		public static ResultType Run(BattingStatus battingStatus, PitchingStatus pitchingStatus, int outcount, Runner runner)
		{
            RandomGacha<ResultType> randomGacha = new RandomGacha<ResultType>(10000);
            foreach (KeyValuePair<ResultType, int> pair in battingStatus.resultProbDistribution) {
                int addValue = pair.Value + pitchingStatus.resultProbDistribution[pair.Key];
                randomGacha.Add(addValue, pair.Key);
            }
			ResultType result = randomGacha.Next();

            if (outcount < 2 && result == ResultType.groundout && runner.IsProbableForceOut() == true) {
                // ゴロアウトを併殺にするかも
                int doubleplayProb = 1 + battingStatus.doubleplay * 5 / battingStatus.plate_appearance;
                int doubleplayRnd = cRandom.Next(0, 100); // 0~99
                if (doubleplayRnd < doubleplayProb) {
                    result = ResultType.doubleplay;
                }
            } else if (outcount < 2 && result == ResultType.groundout && runner.HasRunner(Base.third) == true) {
                // フライアウトを犠飛にするかも
                int sacrificeFlyProb = 25;
                int sacrificeFlyRnd = cRandom.Next(0, 100); // 0~99
                if (sacrificeFlyRnd < sacrificeFlyProb) {
                    result = ResultType.sacrifice_fly;
                }
            }

            return result;
		}
	}
}

