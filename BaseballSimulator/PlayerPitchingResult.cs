﻿using System;

namespace BaseballSimulator
{
	public class PlayerPitchingResult
	{
        
        public int plate_appearance = 0;
        public int getout = 0;
        public int allowed_runs = 0;
        public int earned_runs = 0;
        public int allowed_hits = 0;
        public int allowed_singles = 0;
        public int allowed_doubles = 0;
        public int allowed_triples = 0;
        public int allowed_homeruns = 0;
        public int allowed_sacrifice_bunts = 0;
        public int allowed_sacrifice_flies = 0;
        public int strikeouts = 0;
        public int groundouts = 0;
        public int flyouts = 0;
        public int doubleplays = 0;
        public int bases_on_balls = 0;

        public PitcherStatForPinchHitter GetPitcherStatForPinchHitter() {
            int pitched_inning = (int)Math.Floor((double)getout / 3.0d);
            if (pitched_inning > 9) pitched_inning = 9;
            int tmp_allowed_runs = allowed_runs;
            if (tmp_allowed_runs > 9) tmp_allowed_runs = 9;
            return new PitcherStatForPinchHitter(pitched_inning, tmp_allowed_runs);
        }

	}

    // 投手への代打をするかどうかの判定に使う、投手の投げたイニングや失点状態を表す
    public struct PitcherStatForPinchHitter {

        public int pitched_inning;
        public int allowed_runs;

        public PitcherStatForPinchHitter (int pitched_inning, int allowed_runs) {
            this.pitched_inning = pitched_inning;
            this.allowed_runs = allowed_runs;
        }
    }
}

