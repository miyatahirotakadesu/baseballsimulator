using System;
using System.Collections.Generic;
using System.Linq;

namespace BaseballSimulator
{
	public struct ChangingPlayer
	{
		public int fromPlayerID;
		public int toPlayerID;

        public ChangingPlayer(int fromPlayerID, int toPlayerID)
        {
            this.fromPlayerID = fromPlayerID;
            this.toPlayerID = toPlayerID;
        }

	}

}
