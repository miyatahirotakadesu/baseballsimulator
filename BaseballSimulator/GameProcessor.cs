﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace BaseballSimulator
{
	public class GameProcessor
	{

		int inning = 1; // イニング
		InningStatus inningStatus = InningStatus.top; // 表裏
        int outcount = 0; // アウトカウント
        Runner runner = new Runner();
		Dictionary<TeamType, Team> teams; // ２チーム情報格納
        // 両チームの得点
		Dictionary<TeamType, int> score = new Dictionary<TeamType, int>() { { TeamType.visitor, 0 }, { TeamType.home, 0 } };
        // 両チームの得点 Dictionary<TeamType, Dictionary<イニング, 点数>>
		Dictionary<TeamType, Dictionary<int, int>> scoreHighLight = new Dictionary<TeamType, Dictionary<int, int>>() {
            { TeamType.visitor, new Dictionary<int, int>{} }, { TeamType.home, new Dictionary<int, int>{} }
        };

        List<Play> plays = new List<Play>();

        public static readonly Dictionary<ResultType, int> AddOutcounts = new Dictionary<ResultType, int>() {
            { ResultType.single, 0 }, { ResultType.twobase, 0 }, { ResultType.triple, 0 }, { ResultType.homerun, 0 }, { ResultType.fourball, 0 }, { ResultType.strikeout, 1} , { ResultType.groundout, 1 }, { ResultType.flyout, 1 } , { ResultType.sacrifice_bunt, 1} , { ResultType.sacrifice_fly, 1} , { ResultType.doubleplay, 2 }, { ResultType.foulfly, 1 }
        };

        public void GetTestJson()
        {
            Team hometeam = Team.MakeTestTeam();
            Team visitorteam = Team.MakeTestTeam();
            teams = new Dictionary<TeamType, Team>() { { TeamType.home, hometeam }, { TeamType.visitor, visitorteam } };
            string json = JsonConvert.SerializeObject(teams, Formatting.Indented);
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"./testjson.json"))
            {
                file.WriteLine(json);
            }
            Console.Write("wrote to testjson.json");
        }

		public void TestRun()
		{
            string testjson = TestTeamsJson.GetTestTeamsJson();
            string json = Process(testjson);
            Console.WriteLine(json);
		}

		public void Run(string filename)
		{
            string input_json = System.IO.File.ReadAllText(@filename);
            string output_json = Process(input_json);
            Console.Write(output_json);
		}

        private string Process(string teams_json)
        {
            teams = JsonConvert.DeserializeObject<Dictionary<TeamType, Team>>(teams_json);

            while (ContinueGame()) {

                GameStatus gameStatus = MakeGameStatus();
                List<ChangingOrderStep> fieldingCommand = GetTeam(ODType.defence).CheckPosition(gameStatus);
                ChangingPlayer? reliefPitcher = GetTeam(ODType.defence).DecideReliefPitcher(gameStatus);
                ChangingPlayer? pinchHitter = GetTeam(ODType.offence).DecidePinchHitter(gameStatus);

                TeamStrategy teamStrategy = DecideTeamStrategy(gameStatus);
                Play(fieldingCommand, reliefPitcher, pinchHitter, teamStrategy);
            }

            GameResult gameResult = new GameResult(teams, score, scoreHighLight, plays);
            //string json = JsonConvert.SerializeObject(plays, Formatting.Indented);
            string json = JsonConvert.SerializeObject(gameResult, Formatting.None);
            return json;
        }

        Dictionary<ODType, TeamType> MakeTeamTypeMap()
        {
            Dictionary<ODType, TeamType> team_type_map = new Dictionary<ODType, TeamType>();
            if (inningStatus == InningStatus.top) {
                team_type_map[ODType.offence] = TeamType.visitor;
                team_type_map[ODType.defence] = TeamType.home;
            } else {
                team_type_map[ODType.offence] = TeamType.home;
                team_type_map[ODType.defence] = TeamType.visitor;
            }
            return team_type_map;
        }

        TeamType GetTeamType (ODType odtype)
        {
            Dictionary<ODType, TeamType> team_type_map = MakeTeamTypeMap();
            return team_type_map[odtype];
        }

        Team GetTeam(ODType odtype)
        {
            return teams[GetTeamType(odtype)];
        }

        GameStatus MakeGameStatus()
        {
            int scoreDiff = score[GetTeamType(ODType.offence)] - score[GetTeamType(ODType.defence)];
            int offenceTeamScore = score[GetTeamType(ODType.offence)];
            return new GameStatus(inning, inningStatus, MakeTeamTypeMap(), outcount, runner.GetRunnerStatus(), scoreDiff, offenceTeamScore, scoreHighLight);
        }

        TeamStrategy DecideTeamStrategy(GameStatus gameStatus)
        {
            GameStrategy gameStrategy = new GameStrategy(gameStatus);
            return gameStrategy.DecideTeamStrategy();
        }

        void Play(List<ChangingOrderStep> fieldingCommand, ChangingPlayer? reliefPitcher, ChangingPlayer? pinchHitter, TeamStrategy teamStrategy)
        {
            Player batter = GetTeam(ODType.offence).GetBatter();
            Player pitcher = GetTeam(ODType.defence).GetPitcher();
            BattingOrder battingOrder = GetTeam(ODType.offence).GetBattingOrder();
            Position batterPosition = GetTeam(ODType.offence).positions.First(p => p.Value == battingOrder).Key;
            Dictionary<TeamType, int> playScore = score.ToDictionary(entry => entry.Key, entry => entry.Value);
            Dictionary<Base, int?> playRunner = runner.playerIDs.ToDictionary(entry => entry.Key, entry => entry.Value);
            Play play = new Play(inning, inningStatus, outcount, playScore, playRunner, battingOrder, batterPosition, batter.id, pitcher.id, fieldingCommand, reliefPitcher, pinchHitter, teamStrategy);

            ResultType resultType = ResultMaker.Run(batter.battingStatus.Value, pitcher.pitchingStatus.Value, outcount, runner); // 結果決定
            ResultPlay resultPlay = new ResultPlay(resultType);
            
            int addOutcount = AddOutcounts[resultType];
            int scored = 0;

            if (addOutcount + outcount < 3) {
                AdvanceResult advanceResult = runner.Advance(resultPlay, outcount, batter.id); // 進塁
                scored = advanceResult.homeinCount;
                score[GetTeamType(ODType.offence)] += scored;
                resultPlay.SetScored(scored);
                // スコアボード追記
                if (!scoreHighLight[GetTeamType(ODType.offence)].ContainsKey(inning)) {
                    scoreHighLight[GetTeamType(ODType.offence)].Add(inning, 0);
                }
                scoreHighLight[GetTeamType(ODType.offence)][inning] += advanceResult.homeinCount;
            } else if (addOutcount + outcount < 3) {
                addOutcount = 1;
            }

            outcount += addOutcount;
            resultPlay.DecidePosition();
            GetTeam(ODType.offence).AfterPlay();

            play.AddParam(outcount, resultPlay);

            // 選手成績記録
            batter.SetBattingResult(resultPlay, inning);
            pitcher.SetPitchingResult(resultPlay, addOutcount, scored, scored);

            if (outcount >= 3) {
                ChangeInning();
            }

            plays.Add(play);
        }

        void ChangeInning()
        {
            if (inningStatus == InningStatus.top) {
                inningStatus = InningStatus.bottom;
            } else {
                inning++;
                inningStatus = InningStatus.top;
            }
            outcount = 0;
            runner.ResetRunner();
        }

        bool ContinueGame()
        {
            if (inning < 9) return true;

            bool continueGame = true;

            int batfirst_score = score[TeamType.visitor];
            int fieldfirst_score = score[TeamType.home];

            if (inning > 12) { // 延長12回まで
                continueGame = false;
            } else if (inning >= 9 && inningStatus == InningStatus.bottom) { // 9回裏を進めるかどうか
                continueGame = batfirst_score >= fieldfirst_score;
            } else if (inning >= 10 && !scoreHighLight[TeamType.visitor].ContainsKey(inning)) { // 延長戦に突入や次のイニングに進めるか
                continueGame = batfirst_score == fieldfirst_score;
            } else {
                continueGame = true;
            }

            return continueGame;

        }

	}

}
