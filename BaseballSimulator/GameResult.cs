﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BaseballSimulator
{
	public struct GameResult
	{
        public Dictionary<TeamType, GameResultTeam> teams;
        public Dictionary<TeamType, int> score;
		public Dictionary<TeamType, Dictionary<int, int>> scoreHighLight;
        public List<Play> plays;

        public GameResult(Dictionary<TeamType, Team> teams, Dictionary<TeamType, int> score, Dictionary<TeamType, Dictionary<int, int>> scoreHighLight, List<Play> plays)
        {
            this.teams = teams.ToDictionary(entry => entry.Key, entry => new GameResultTeam(entry.Value));
            this.score = score;
            this.scoreHighLight = scoreHighLight;
            this.plays = plays;
        }

	}

    public struct GameResultTeam
    {
		public string id;
		public Dictionary<BattingOrder, int> order;
		public Dictionary<Position, BattingOrder> positions;
		public Dictionary<int, BenchRole> benchPlayerIds;
		public List<Player> players;

        public GameResultTeam(Team team)
        {
            this.id = team.id;
            this.order = team.order;
            this.positions = team.positions;
            this.benchPlayerIds = team.benchPlayerIds;
            this.players = team.players;
        }
    }
}

